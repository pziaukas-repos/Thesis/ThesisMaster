clear;
close all;
load('../calc/NewtonWD.mat');

% Zones of interest
minD = 0.4;
maxD = 1;
minW = 0;
maxW = 0.6;

zoom = 2;

D = mat2gray(interp2(D,zoom));
W = mat2gray(interp2(W,zoom));
R = interp2(R,zoom);

M = zeros(size(R));
M(D >= minD & D <= maxD & W >= minW & W <= maxW) = 1;
numM = sum(M(:));
fprintf('Coverage: %.3f%%\n', 100*numM/numel(M));

Paint(M);
if (numM > 0);
    figure;
    indices = find(M == 1, randi(numM), 'first');
    p = R(indices(end));
    fZ = Newton(Z, n, p);
    A = NewtonAttractors(fZ);
    Paint(A);
end;