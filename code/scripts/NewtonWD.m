isCalculated = true;
switch isCalculated;

    
    
% Start of SHOW action
case true;
% Preparing data
clear;
load('../calc/NewtonWD.mat');
zoom = 2;

D = interp2(D,zoom);
W = interp2(W,zoom);
R = interp2(R,zoom);
PaintMap(W);
PaintMap(D);

IN = round(ginput(1));
fZ = Newton(Z, n, R(IN(2),IN(1)));
A = NewtonAttractors(fZ);
figure(2);
Paint(A);
% End of SHOW action



% Start of CALC action
case false;
clear;

% Hard parameters
xMin = -1; xMax = 1;
yMin = -1; yMax = 1;
d = 0.0025;
n = 30;

pMin = -1; pMax = 1;
qMin = -1; qMax = 1;
h = 0.01;

[X, Y] = meshgrid(xMin:d:xMax, yMax:-d:yMin);
Z = X + 1i * Y;
[P, Q] = meshgrid(pMin:h:pMax, qMax:-h:qMin);
R = P + 1i * Q;
sR = size(R);

D = zeros(sR);
W = zeros(sR);
for row = 1:sR(1);
    for col = 1:sR(2);
        fZ = Newton(Z, n, R(row,col));
        A = NewtonAttractors(fZ);
        D(row,col) = Dimension(A);
        W(row,col) = Wada(A);
    end;
    fprintf('Script Newton WD: %d%% done\n',round(100*row/sR(1)))
end;

save('../calc/NewtonWD.mat');
% End of CALC action

end;