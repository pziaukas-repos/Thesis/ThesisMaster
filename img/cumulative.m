clear;
load('../calc/NewtonWD.mat');
steps = 300;

zoom = 2;
D = interp2(D,zoom);
W = interp2(W,zoom);

for k = 3:4;
switch k;
    case 1;
        str = 'D';
        minArg = 1.103; 
        maxArg = max(D(:));
        h = (maxArg - minArg) / steps; % re-definition
        for step = steps:-1:0;
            nowArg = minArg + step*h;
            maxRez(step+1) = max(W(D <= nowArg));
            minRez(step+1) = min(W(D >= nowArg));
        end;
        plot(minArg:h:maxArg, smooth(maxRez, 5), 'k', 'LineWidth', 2);
        hold on;
        plot(minArg:h:maxArg, smooth(minRez, 5), 'r', 'LineWidth', 2);
        hold off;
        xlabel('$D(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$W(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
    case 2;
        str = 'CD';
        clear minRez maxRez;
        for step = steps:-1:0;
            nowArg = minArg + step*h;
            maxRez(step+1) = max(W(D <= nowArg & D > nowArg-h));
            minRez(step+1) = min(W(D <= nowArg & D > nowArg-h));
        end;
        plot(minArg:h:maxArg, smooth(maxRez, 5), 'k', 'LineWidth', 2);
        hold on;
        plot(minArg:h:maxArg, smooth(minRez, 5), 'r', 'LineWidth', 2);
        hold off;
        xlabel('$D(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$W(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
    case 3;
        str = 'W';
        clear minRez maxRez;
        minArg = 0.25; 
        maxArg = max(W(:));
        h = (maxArg - minArg) / steps; % re-definition
        for step = steps:-1:0;
            nowArg = minArg + step*h;
            maxRez(step+1) = max(D(W <= nowArg));
            minRez(step+1) = min(D(W >= nowArg));
        end;
        plot(minArg:h:maxArg, smooth(maxRez, 5), 'k', 'LineWidth', 2);
        hold on;
        plot(minArg:h:maxArg, smooth(minRez, 5), 'r', 'LineWidth', 2);
        hold off;
        xlabel('$W(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$D(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
    case 4;
        str = 'CW';
        clear minRez maxRez;
        for step = steps:-1:0;
            nowArg = minArg + step*h;
            maxRez(step+1) = max(D(W <= nowArg & W > nowArg-h));
            minRez(step+1) = min(D(W <= nowArg & W > nowArg-h));
        end;
        plot(minArg:h:maxArg, smooth(maxRez, 5), 'k', 'LineWidth', 2);
        hold on;
        plot(minArg:h:maxArg, smooth(minRez, 5), 'r', 'LineWidth', 2);
        hold off;
        xlabel('$W(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$D(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
end;

daspect('auto');
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.1 0.22 0.88 0.75],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') str '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 15 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 5 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;