function PaintMap(M)
% Paint the map

figure('units','normalized','outerposition',[0 0 1 1]);
imshow(uint8(255*mat2gray(M)));
colormap jet;
mMin = min(M(:));
mMax = max(M(:));
colorbar('YTickLabel', {mMin:.2*(mMax-mMin):mMax});

end