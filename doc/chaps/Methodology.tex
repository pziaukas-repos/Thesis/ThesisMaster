\chapter{Methodology}
The purpose of a more detailed methodology is to present the ideas and algorithms that are originally applied in this work.

Section \ref{sec:NDDS} is dedicated to the discrete Newtonian dynamical system and its generalizations.
Section \ref{sec:FD} introduces box counting algorithm which is used to evaluate the fractal dimension of a basin boundary.
Lastly, section \ref{sec:WM} presents a novel algorithm for measuring Wada property of some basins of attraction.



\section{Newton's discrete dynamical system} \label{sec:NDDS}
Basic Newton's method (also known as the Newton-Raphson method) is one of the most influential methods for finding the roots of a real-valued polynomial $p(x)$~\cite{Atkinson1978}. 

\begin{figure}[ht]
	\centering
	\includegraphics{../img/basinBig100_000i}
	\caption[Basins of attraction for the Newton's discrete dynamical system]
	{Basins of attraction for the NDDS with $p(z)=z^3-1$. 
		Each color corresponds to the different attractor 
		$\cA_k$, where $k \in \{ \, 1,2,3 \,\}$.}
	\label{fig:basinbig1}
\end{figure}

After the method was established in the seventeenth century, modern interpretations and implementations began to differ substantially from the original version (see subsection~\ref{sec:NDDSapps} for various applications).

In the nineteenth century Schröder~\cite{Schrder1870} and Cayley~\cite{Cayley1879} came up with problems and solutions that eventually led to the formal generalization of the Newton's method for a polynomial $p(z)$ with complex coefficients.
In this work it is referred to as the Newton's discrete dynamical system.



\subsection[Introduction]{Introduction to Newton's discrete dynamical system}
A Newton's dynamical system $f \colon \bC \to \bC$ is a discrete dynamical system fully described by its iterations $z_{k+1} = f(z_k)$, where $z_k,z_{k+1} \in \bC$.

\begin{mydef}
For a given complex polynomial $p(z)$ a single {\bfseries Newton's iteration} at a point $z \in \bC$ is defined as
\begin{equation}
f_p(z) = z - \frac{p(z)}{p'(z)}.
\end{equation}
\end{mydef}

Once the evolutionary process is fully described, the discrete Newton's dynamical system (NDDS for short) corresponding to some complex polynomial $p(z)$ can be considered.
The behavior of the aforementioned dynamical system is characterized by orbits 
$\{\, z_0,\ z_1,\ z_2,\ \ldots \,\}$. 
Each orbit is determined unambiguously by the initial seed $z_0 \in \bC$ using the known iterative process
\begin{equation}
z_n = f_p^{\circ n} (z_0), \quad \forall n \in \bN.
\end{equation}
The emphasis on a general polynomial $p$ is omitted during the further analysis. 
The third degree polynomial $p(z) = z^3 - 1$ is taken by default, unless specified otherwise.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basinP2}
		\caption{$p(z) = z^4-z;$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basinP5}
		\caption{$p(z)= z^5-1.$}
	\end{subfigure}
	\caption[Basins of attraction for the NDDS with polynomials of higher order]
	{Basins of attraction for the NDDS with polynomials of higher order. 
		Different polynomials $p(z)$ are used.}
	\label{fig:basinsP}
\end{figure}

In figure~\ref{fig:basinbig1} basins of attraction $\cB_k$, where $k \in \{ \, 1,2,3 \,\}$, of the NDDS are calculated and presented visually using the default polynomial $p(z) = z^3 - 1$.
It is apparent that the basins are intertwined in a complex manner and further investigations are needed.

Polynomials of higher order $p(z) = z^4-z$ and $p(z)= z^5-1$ are also assigned to NDDS and presented visually in figure~\ref{fig:basinsP}.
In this case similarly complex processes are observed near the boundary of basins $\cBo$.

A variation of escape time algorithm (see subsection \ref{sec:Feta}) is used for calculations.
The appropriate attractor is assigned to an initial seed $z_0 \in \bC$ after $n=30$ iterations. 




\subsection[Properties]{Properties of Newton's discrete dynamical system}
\subsubsection{Attractors}
Newton's discrete dynamical system $f_p$ has some attractors $\cA \subset \bC$, provided that the polynomial $p$ is non-constant.
The set of attractors comprises the fixed points satisfying the definition $\{\, r \in \bC \mid p(r)=0 \,\}$. 
Consequently, it is important to understand the basins of attraction $\cB$ for each attractor - the structure, properties and dynamics of these peculiar sets~\cite{Drexler1996a}.

\begin{figure}[ht]
	\centering
	\includegraphics{../img/basinBigH}
	\caption[$H$-ranks for the Newton's discrete dynamical system]
	{$H$-ranks for the NDDS with $p(z)=z^3-1$.}
	\label{fig:basinbigh}
\end{figure}



\subsubsection{Basin boundaries}
In the twentieth century Fatou~\cite{Fatou1919} and Julia~\cite{Julia} made some independent discoveries regarding the basins of attraction $\cB$ and especially the boundaries of these basins $\cBo$ that are present in NDDS.
If there are multiple Fatou domains (basins) $F_k = \cB_k$, for some $k = \ival{1}{n}$, then each point of the Julia set (boundary) $J = \cBo$ must contain points from multiple basins in its neighborhood.
If there are more than two different basins of attraction $n>2$, then the Julia set is not a simple curve but rather a fractal shape $J = \cF$~\cite{Hubbard2001}.



\subsubsection{Speed of convergence}
Another important aspect is the speed of convergence that is characterized by $H$-ranks.
In figure~\ref{fig:basinbigh} Hankel ranks are calculated using the default polynomial $p(z) = z^3 - 1$.
Since the convergence of a Julia set $J$ cannot be predicted, Hankel ranks are substantially higher in the vicinities of the basin boundary $\cBo$.
It also shows that the system can be very sensitive to its choice of starting points outside of certain regions.



\subsection[Generalization]{Generalization of Newton's discrete dynamical system}
NDDS itself is a classical tool whose typical speed of convergence varies from quadratic to linear depending on initial conditions~\cite{Holt1984}.
A more general case is considered in order to gain additional flexibility
\begin{equation} \label{eq:rNDDS}
f_p(z) = z - \alpha \frac{p(z)}{p'(z)}.
\end{equation}
The modified Newtonian iteration \eqref{eq:rNDDS} introduces an arbitrary coefficient $\alpha \in \bC$ instead of a constant value $\alpha = 1$.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basin050_000i}
		\caption{$\alpha = 0.5;$}
		\label{fig:basins_lo}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basin200_000i}
		\caption{$\alpha = 2.$}
		\label{fig:basins_hi}
	\end{subfigure}
	\caption[Basins of attraction for the relaxed Newton's discrete dynamical system]
	{Basins of attraction for the relaxed NDDS with $p(z)=z^3-1$. 
		Different parameters $\alpha \in \bR$ are used.}
	\label{fig:basins_lo-hi}
\end{figure}

This generalization is called a relaxed Newton's discrete dynamical system~\cite{GILBERT2001}. 
It introduces a quantitative change in the speed of convergence as well as a qualitative change in the basins of attraction for the NDDS, even for $\alpha \in \bR$.
Typically $0 < \alpha < 1$ softens the fractal pattern of the basin boundary while performing smaller changes in variable during every iteration (see fig.~\ref{fig:basins_lo}).
On the other hand, $\alpha > 1$ typically sharpens the fractal pattern while providing bigger and more aggressive changes in variable (see fig.~\ref{fig:basins_hi}). 
Overall this effect can be clearly seen in figure~\ref{fig:basins_lo-hi}.

\subsection[Applications]{Applications of Newton's discrete dynamical system} \label{sec:NDDSapps}
NDDS can be used to analyze dynamical systems of varying degree of difficulty. 
In~\cite{Holt1984} Newton's method is considered as an iterative scheme to compute the complicated basins of attraction for some discrete dynamical systems.
The chaotic number of iterations needed by Newton’s method to converge to its attractors is discussed in~ \cite{Susanto2009}.
A general behavior of Newton's method for cubic polynomials is investigated in~\cite{Walsh1995}, including the emergence of period-three orbits.

The emergence of fractals in NDDS is well-acknowledged amongst contemporary researchers.
Both theoretical and experimental evidence of fractal characteristics of Newton‘s method is presented in~ \cite{Drexler1996}.
Another overview of Newton‘s method and the convergence to fractal patterns is published in~\cite{Jr1991}.
Some peculiar examples of fractals obtained using Newton’s method are presented in~ \cite{Cartwright1999}.
The theory for stabilization of Newton‘s method in order to eliminate fractal basin boundaries is developed in~ \cite{Drexler1995}.
Implications of the fractal basin boundaries generated by Newton’s method for the aero-elastic analysis of a helicopter motion is discussed in~ \cite{ChandraSekhar2010}.

Complicated relations between the basins of attraction lead to peculiar applications.
An in-depth analysis of interwoven basins of attraction using a damped Newton’s method is discussed in~ \cite{Epureanu1998}.
A graphical presentation of interlacement amongst three different basins of attraction is Newton’s method is presented in~ \cite{Frame2007}.

A variety of alternatives and modifications to Newton's method are known.
A modification of Newton’s method using some novel adaptive step size control procedure is proposed in~ \cite{Amrein2014}.
Standard Newton’s method, Halley’s method, and Schroder’s method is compared in terms of structural characteristics of Julia sets in~ \cite{Wang2007}.
A variety of different possible generalizations of Newton’s method is reviewed in~ \cite{GILBERT2001}.



\section{Fractal dimension} \label{sec:FD}
\subsection[Introduction]{Introduction to fractal dimension}
For a lot of usual and familiar objects the notion of dimensionality is very intuitive and straightforward. 
For example, a manifold is $D$-dimensional if locally it resembles a Euclidean space of dimension $D$, which obviously has an integer value \cite{Lee2000}.
However, sometimes an object does not conform to any integer dimension.
In that case, some more general tools are needed in order to understand and evaluate the dimension.

Ideally, a generalized definition of dimension $D$ should satisfy the following requirements:
\begin{enumerate}
	\item Points as well as countable unions of points $X$ have $D(X) = 0$.
	\item Manifolds $M$ have an integer dimension $D(M) \in \bN$ which coincides with the usual notion of topological dimension.
	\item General sets $\cF$ may have some fractional dimension $D(\cF) \notin \bZ$.
\end{enumerate}

Unfortunately, it can be exceptionally hard to determine the precise fractal dimension of a given entity $\cF$ in general case. 
Numerical approximations are typically used instead.
The following subsections are dedicated to this matter in order to give some deeper insights regarding the problem.



\subsection[Concept]{Concept of fractal dimension}
There are multiple characteristics that may be referred to as fractal dimensions.
These measures provide an objective means to compare sets in terms of various features. 

Box counting dimension (also known as Minkowski or Minkowski–Bouligand dimension) provides a way of determining the fractal dimension of a set $\cF$ in any metric space. 
However some more preliminary definitions are necessary before further introduction.

\begin{mydef} \label{def:box}
The {\bfseries box} of side length $\varepsilon$ around the point $z_0 \in \bC$ is defined
\begin{equation}
B(\varepsilon, z_0) = \left\{ z \in \bC \left| \Re (z_0) - \frac{\varepsilon}{2} \le \Re (z) \le \Re (z_0) + \frac{\varepsilon}{2},
\Im (z_0) - \frac{\varepsilon}{2} \le \Im (z) \le \Im (z_0) + \frac{\varepsilon}{2} \right. \right\}.
\end{equation}
\end{mydef}
The box defined above is actually a closed ball described using Manhattan metric~\cite{Krause1975} instead of a more typical Euclidean one.
Nevertheless, it is useful when working in discrete grids which typically occur in computational environments.

\begin{mydef} \label{def:N}
The {\bfseries minimal number of boxes} of side length $\varepsilon$ that are needed to cover a given closed and compact set $\cF$ is denoted
\begin{equation}
N(\varepsilon) = \min \left\{ n \in \bN \left| \cF \subset \bigcup_{k=1}^n B(\varepsilon, z_k), z_k \in \bC \right. \right\}.
\end{equation}
\end{mydef}
Such a number will always be found in case of $\cF$ being compact because it follows from the very definition of compactness that there exists a finite subcover of (open) balls.
Amongst these well-defined and finite coverings some (or at least one) will be minimal.

Now a particular proposed dimension $D$ is related to the number $N$ such that
\begin{equation}
N(\varepsilon) \approx C \varepsilon^{-D}, \quad \text{for some } C>0;
\end{equation}
and the above approximation becomes more precise as the multiplier $\varepsilon$ decreases.
Then it is natural to solve for $D$ by taking the logarithm to the base $e$ and obtain
\begin{equation} \label{eq:DwithC}
D \approx \frac {\log N(\varepsilon) - \log C}{\log (1/\varepsilon)}.
\end{equation}

Since the evaluation of dimension in question involves counting the number of $\varepsilon$-sized boxes $N(\varepsilon)$, it suffices to have a reasonable approximation of $\cF$ and work at a fine precision $\varepsilon$.
Then the second summand in the numerator of \eqref{eq:DwithC} disappears and this leads to the following definition.
\newpage

\begin{mydef} \label{def:D}
The {\bfseries box counting dimension} (or simply dimension) $D$ of a compact non-empty set $\cF \subset \bC$ is defined as
\begin{equation} \label{eq:D}
D(\cF) = \lim_{\varepsilon \to 0} \frac {\log N(\varepsilon)}{\log (1/\varepsilon)};
\end{equation}
where $N(\varepsilon)$ is the number of $\varepsilon$-sized boxes that are required to cover $\cF$. \qed
\end{mydef}

\begin{figure}[ht]
	\centering
	\includegraphics{../img/basinBigEdge100_000i}
	\caption[Basin boundaries for the Newton's discrete dynamical system]
	{Basin boundary $\cF = \cBo$ for the NDDS with $p(z)=z^3-1$.}
	\label{fig:basinbigedge1}
\end{figure}

It is possible to extend the definition~\ref{def:D} and provide a value of dimension for a wider variety of sets.
It is possible to use the following superior limit
\begin{equation} \label{eq:genD}
D(\cF) = \lim_{\varepsilon \to 0} \sup \frac {\log N(\varepsilon)}{\log (1/\varepsilon)}.
\end{equation}
It can be proved that the above two presentations \eqref{eq:D} and \eqref{eq:genD} are mathematically consistent~\cite{Barnsley2000}.
However, the latter is obviously more general. 
This broader definition \eqref{eq:genD} can be applied in some more complicated cases where the first one fails.
However, in the scope of this work it is sufficient to use the former approach \eqref{eq:D}.

\begin{myex}
Let's consider a unit square $\cF = \{ \, z \in \bC \mid 0 \le \Re(z) \le 1, 0 \le \Im(z) \le 1 \, \} $ which intuitively has dimensionality equal to $2$.
Take a decreasing positive sequence 
\begin{equation*}
\begin{pmatrix}
1 & 1/2 & 1/4 & \ldots & 1/2^n & \ldots
\end{pmatrix}.
\end{equation*}
It is understandable that for this set $\cF$ the associated box counts
\begin{align*}
&N(1) = 1,\\
&N(1/2) = 4,\\ 
&N(1/4) = 16, \quad \text{and generally} \\
&N(1/2^n) = 4^n.
\end{align*}
The expression \eqref{eq:D} is applied in order to obtain the fractal dimension
\begin{equation*}
D(\cF) = \lim_{n \to +\infty} \frac {\log N(1/2^n)}{\log (2^n)} = \lim_{n \to +\infty} \frac {\log (4^n)}{\log (2^n)} 
= \frac {\log 4}{\log 2} = 2.
\end{equation*}
\end{myex}

\begin{myex}
It can be approximated using the box counting algorithm (see~\cite{Drexler1996a}) that the fractal dimension of the basin boundary seen in figure~\ref{fig:basinbigedge1} is equal to $D(\cF) = 1.437$.
\end{myex}



\subsection[Algorithm]{Algorithm to approximate fractal dimension} \label{sec:algD}
In short, the conventional idea of approximation involves scanning a non-overlapping $\varepsilon$-sized grid, finding the portions containing $\cF$, and making conclusions according to the definition of box counting dimension. 

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/boxCountGrid40}
		\caption{$\varepsilon = 0.1;\ N(\varepsilon) = 145;$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/boxCountGrid20}
		\caption{$\varepsilon = 0.05;\ N(\varepsilon) = 355.$}
	\end{subfigure}
	\caption[The principle of box counting using boxes of various sizes $\varepsilon$.]
	{The principle of box counting using boxes of various sizes $\varepsilon$.
		The digital representation of $\cF$ is split into an $\varepsilon$-grid, then count the nodes where the basin boundary $\cBo$ is present.}
	\label{fig:boxCountGrid}
\end{figure}

In reality the ability to reduce $\varepsilon$ is limited. For digital images (the representation of fractal structures) it is not possible to zoom more than to an individual pixel.
The proposed solution:
\begin{enumerate}
	\item Look at some finest $\varepsilon \in S$ values. \\
	These should initially cover $1 \times 1$ grid of pixels, then $2 \times 2$ grid pixels and so on (see fig.~\ref{fig:boxCountGrid}).
	\item Calculate the observations $\log N(\varepsilon)$ and $\log (1/\varepsilon)$ in each case. \\
	This step is self-explanatory. The definition \ref{def:N} is applied.
	\item Approximate the slope using the least square linear regression or an alternative method. \\
	Assume that some pairs of observations $(x_k, y_k)$, $k = \ival{1}{n}$, are supposed to be related $y_k = \gamma x_k+C$ up to some level of disturbance.
	Here $\gamma$ is the slope and $C$ is the constant term. 
	Then the evaluations that minimize root-mean-square error (RMSE) are
	\begin{align}
	&C = \frac{\sum_{k=1}^n y_k - \gamma \sum_{k=1}^n x_k}{n}; \\
	&\gamma = \frac{n \sum_{k=1}^n(x_k y_k) - (\sum_{k=1}^n x_k)(\sum_{k=1}^n y_k)}{n \sum_{k=1}^n x_k^2 - (\sum_{k=1}^n x_k)^2}.
	\end{align}
\end{enumerate}

After aforementioned procedures the approximation of fractal dimension $D$ may be expressed
\begin{equation}
D(\cF) \approx \frac{
|S| \sum_{\varepsilon \in S}{\log N(\varepsilon) \log (1/\varepsilon)} - 
\left( \sum_{\varepsilon \in S}{\log N(\varepsilon)} \right) \cdot
\left( \sum_{\varepsilon \in S}{\log (1/\varepsilon)} \right)
}{
|S| \sum_{\varepsilon \in S}{\log^2 (1/\varepsilon)} -
(\sum_{\varepsilon \in S}{\log (1/\varepsilon)})^2}.
\end{equation}
This approximation is used in further research and analysis.



\subsection[Applications]{Applications of fractal dimension}
Many various real-world phenomena exhibit some fractal properties.
These properties are well described using the concept of fractal dimension.
In neuroscience, different cell types apparently have significantly different dimensions~\cite{Caserta1995}.
In medicine, the dimension is useful for studies of X-ray and MR images~\cite{Fortin1992}.
Overall, the fractal dimension is an important tool for studies of natural patters and textures~\cite{Chaudhuri1995}.

On the other hand, applications of fractal dimension are directly related to applications of fractals themselves.
These aspects are discussed in detail in subsection~\ref{sec:Fapps}.



\section{Wada measure} \label{sec:WM}
\subsection[Introduction]{Introduction to Wada characteristic}
In theoretical mathematics, there exists a concept called lakes of Wada.
It occurs in a plane which is divided into three disjoint connected open sets that have a peculiar and highly counterintuitive property: they all share the same boundary.

In applied mathematics, there exists a variety of vastly different nonlinear dynamical systems describing both natural and unnatural phenomena.
Typically, these systems possess multiple nontrivial attractors. 
It is therefore understandable that the principles of Wada are defined in terms of basins of attraction and their respected boundaries, which are shared.

\begin{myex} \label{ex:wada}
Let's construct a region that has full Wada property.
For that purpose, three different basins of attraction $R$, $Y$, $B$ are used.
The basins correspond to colors red, yellow and blue respectively.
\begin{enumerate}
	\item Without loss of generality it can be said that the region is rectangular. \\
	Initially all points in the region are undefined (not assigned to any basin of attraction).
	\item The undefined region is divided vertically into three equal parts. \\
	Side parts are assigned to two different basins of attraction; the middle part is left undefined.
	\item The undefined parts are divided vertically into three equal even smaller parts. \\
	Side parts are left undefined; the middle part is assigned to the basin which is not nearby.
	\item Step 3 is repeated indefinitely.
\end{enumerate}

\begin{figure}[ht]
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics{../img/wada0}
		\caption{Iteration $n = 1$;}
	\end{subfigure} 
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics{../img/wada1}
		\caption{Iteration $n = 2$;}
	\end{subfigure}
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics{../img/wada2}
		\caption{Iteration $n = 3$;}
	\end{subfigure}
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics{../img/wada3}
		\caption{Iteration $n = 4$.}
	\end{subfigure}
	\caption[Emergence of Wada property]
	{Emergence of Wada property. A region is divided into three pieces, side pieces are assigned to two different basins of attraction, the middle piece is undefined. In every subsequent turn an undefined portion is again divided into three pieces, side pieces are undefined, the middle piece is assigned to the basin of attraction which is not nearby.}
	\label{fig:wada_sch}
\end{figure}

As the number of repeated iterations $n$ increases, the set of undefined points is getting smaller.
In the limit case $n \to +\infty$, a whole region of points is assigned to some basins $R$, $Y$, $B$.
Moreover, all these basins have a common boundary, thus the full Wada property.
The whole process is illustrated in figure~\ref{fig:wada_sch}.
\end{myex}



\subsection[Concept]{Concept of Wada measure}
As already discussed, the situation when every boundary point neighbors more than two distinct basins of attraction $\cB_k$, $k \in \ival{1}{n}$, is known as the Wada property~\cite{Kennedy1991}.
If the system has exactly three different attractors $\cA_1,\ \cA_2,\ \cA_3$, it follows that all basins of attraction $\cB_1,\ \cB_2,\ \cB_3$ have a common boundary $\cBo$.
Some more preliminary definitions are needed before proceeding with quantitative approximations of Wada measure.

\begin{mydef}
The {\bfseries number of $n$-boxes} $N_n (\varepsilon)$ is the minimal number of boxes (according to the definition \ref{def:N}) that satisfy an additional property
\begin{equation}
B(\varepsilon, z) \cap \cB_k \neq \varnothing, \quad \text{for all } k \in \{ \, k_1,k_2,\ldots, k_n \, \};
\end{equation}
for a fixed side length $\varepsilon$ and compact nonempty set $\cF$. $\qed$
\end{mydef}

Simply put, the number of $n$-boxes counts the size of (partial) $\cF$ covering which contains points from at least $n$ different basins of attraction.
Then the following inequality holds
\begin{equation}
0 \le N_n(\varepsilon) \le N(\varepsilon), \quad \forall n \in \bN,\ \forall \varepsilon > 0.
\end{equation}

Let's look at the case where a given dynamical system has exactly three attractors and three corresponding basins of attraction $\cB_1,\ \cB_2,\ \cB_3$ more closely.
In extreme cases where the basins are relatively far away and form distinct boundaries it is reasonable to expect $N_3(\varepsilon) \approx 0$, 
whereas if basins are intertwined and close together it should be $N_3(\varepsilon) \approx N(\varepsilon)$.

Finally, the Wada measure can be defined with precision.
\begin{mydef}
A {\bfseries Wada measure} $W$ for a compact non-empty set $\cF \subset \bC$ is defined as
\begin{equation}
W(\cF) = \lim_{\varepsilon \to 0} \frac{N_3(\varepsilon)}{N_2(\varepsilon)} = \lim_{\varepsilon \to 0} \frac{N_3(\varepsilon)}{N(\varepsilon)}
\end{equation}
where $N(\varepsilon)$ and $N_3(\varepsilon)$ is the number of $\varepsilon$-sized boxes that cover $\cF$. \qed
\end{mydef}

In terms of the classical theory of probability, the Wada measure is the probability of a random point taken from boundary $\cBo$ having at least three different basins of attraction in its immediate neighborhood.
If the region of interest has full Wada property, then its boundary $\cBo$ has a theoretical Wada measure $W(\cBo) = 1$.
On the contrary, if the region does not resemble Wada situation at all, then $W(\cBo) = 0$.




\subsection[Algorithm]{Algorithm to approximate Wada measure} \label{sec:algW}
Once again, the idea involves scanning a non-overlapping $\varepsilon$-sized grid, finding the portions containing $\cF$, counting the number of basins of attraction that they contain, and making conclusions according to the definition of Wada measure. 

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/boxCount40}
		\caption{$\varepsilon = 0.1;\ N_3(\varepsilon) = 143;\ N(\varepsilon) = 145;$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/boxCount20}
		\caption{$\varepsilon = 0.05;\ N_3(\varepsilon) = 342;\ N(\varepsilon) = 355.$}
	\end{subfigure}
	\caption[The principle of Wada measure using boxes of various sizes $\varepsilon$.]
	{The principle of Wada measure using boxes of various sizes $\varepsilon$.
		The digital representation of $\cF$ is split into an $\varepsilon$-grid, then count the nodes where the basin boundary $\cBo$ is present and count the occurrences from different basins of attraction.}
	\label{fig:boxCount}
\end{figure}

The ability to reduce $\varepsilon$ is similarly limited to no more than an individual pixel.
The proposed solution is this:
\begin{enumerate}
	\item Look at some finest $\varepsilon \in S$ values. \\
	These should initially cover $2 \times 2$ grid of pixels, then $3 \times 3$ grid pixels and so on (see fig.~\ref{fig:boxCount}).
	\item Calculate the observations $N(\varepsilon)$ and $N_3(\varepsilon)$ in each case. \\
	The empirical probability of any given box being a $3$-box is $N_3(\varepsilon) / N(\varepsilon)$.
	\item Take the mean empirical probability of the observations. 
\end{enumerate}

After aforementioned procedures the outcome may be expressed as
\begin{equation}
W(\cF) \approx \frac{1}{|S|} \sum_{\varepsilon \in S} \frac{N_3(\varepsilon)}{N(\varepsilon)}.
\end{equation}
Realistically, the approximated value of $W(\cF)$ is never quite equal to $1$. 
On the other hand, it serves its purpose of measuring and comparing Wada characteristics of different dynamical systems and situations.
This approximation is mainly used in further research and analysis.



\subsection[Applications]{Applications of Wada characteristic}
Rigorous theorems and important statements regarding Wada basin boundaries and basin cells are presented in~ \cite{Nusse1996}.
Wada property for different types of attractors including strange nonchaotic attractors is discussed in~ \cite{Zhang2012}.
Sufficient and necessary conditions guaranteeing that three Wada basins are emerging from a tangent bifurcation are presented in~ \cite{Breban2005}.

Wada property emerges in a variety of systems of high interest in physics.
Unpredictable behavior of Wada basin boundaries in the Duffing oscillator is noted in~ \cite{Aguirre2002}.
Wada property in some systems of chaotic scattering with multiple exit modes is analyzed in~ \cite{POON1996}.
Topological characteristics, including Wada property, are considered for some systems of chaotic scattering in~ \cite{Sweet1999}.
Fractal and – more specifically – Wada exit basin boundaries are analyzed in a tokamak system in~ \cite{PORTELA2007}.

Basins are highly interwoven even in non-standard situations.
Seemingly unexpected situations where basins of attraction have Wada property are revealed in~ \cite{Kennedy1991}.

Wada effect is present in nature.
Unpredictability of ecological models related to Wada basins is interpreted graphically in~ \cite{Vandermeer2004}.
Examples of Wada characteristic in a periodically forced Lotka-Volterra predator-prey model are found in~ \cite{Vandermeer2001}.

Modifications and generalizations of Wada quality are proposed by contemporary researchers.
Transitions from totally Wada basins to partially Wada basins and vice versa are constructed in~ \cite{Zhang2013}.
A novel method for testing for basins of Wada is proposed in~ \cite{Daza2015a}.
