clear;

xMin = -1.5; xMax = 0.5;
yMin = -1; yMax = 1;
d = 0.001;
n = 50;

[X, Y] = meshgrid(xMin:d:xMax, yMin:d:yMax);
Z = zeros(size(X));
C = X + 1i*Y;
for k = 1:n;
    Z = Z.^2 + C;
end;

imshow(abs(Z)>2, 'XData', xMin:d:xMax, 'YData', yMin:d:yMax);
daspect('auto');
xlabel('$\Re(z)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
ylabel('$\Im(z)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.14 0.12 0.82 0.82],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 12 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 12 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);
