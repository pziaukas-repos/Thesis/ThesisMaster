clear;
close all;
load('../calc/NewtonWD.mat');
addpath('../code');

z0 = -0.793;
rad = .01;

Z = z0 + rad*[-1+1i 1+1i; -1-1i 1-1i];
Z = interp2(Z,5);
aroundZ = abs(Z - z0) <= rad;
% imshow(aroundZ);

for r = [.97 1 1+.05*1i];
fZ = Newton(Z(aroundZ), n, r);
A = NewtonAttractors(fZ);
figure;
PaintRadar(A,5);

set(gca,...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [-0.09 -0.05 1.02 1.02],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 5 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 5 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');

fname = [mfilename('fullpath') sprintf('%03d_%03di', round(100*real(r)), round(100*imag(r))) '.eps'];
print(gcf, '-r300', '-depsc', fname);
end;