function A = NewtonAttractors( Z, varargin )
% Nearest attractors
% NewtonAttractors(Z, [P])
% of arguments Z
% for the polynomial P

optargs = {[1 0 0 -1]};
optargs(1:nargin-1) = varargin;
[P] = optargs{:};

S = size(Z);
R = roots(P);

diffs = repmat(Z(:),1,numel(R))-repmat(R',numel(Z),1);
[~, A] = min(abs(diffs),[],2);
A = reshape(A, S);
A(isnan(Z)) = 0;

end