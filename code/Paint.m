function Paint( A, varargin )
% Painting the map of discrete values
% Paint(A) - 2D
% Paint(A, t) - 3D

optargs = {0};
optargs(1:nargin-1) = varargin;
[t] = optargs{:};

cmap = [.9 .1 .1; .9 .9 .1; .1 .1 .9; 1 1 1];

imshow(label2rgb(A(:,:,t+1), cmap, 'k'));
axis on;


end

