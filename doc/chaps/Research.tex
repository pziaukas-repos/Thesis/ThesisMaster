\chapter{Research}
In this chapter that is dedicated to research the original numerical results and calculations are presented.
All the following insights are directly related to the preliminaries, methods and mathematical tools described up to this point. 

Section \ref{sec:BB} reveals the analysis of basin boundaries. 
It is split into three important parts: analysis of dimensionality, Wada measure, and the relation between both of them.

Section \ref{sec:I} presents implications of chaos in the present setting.
Once again it is divided into two subsections where different aspects are discussed. 
The first one considers the evaluation of uncertainty, the second one considers the control of the dynamical system.

\section{Analysis of basin boundaries} \label{sec:BB}
Artificially generated visual representations of attractors and basins of attraction are amongst typical choices for researchers who seek to describe and analyze the behavior of some dynamical systems~\cite{Ott2002}. 
Therefore, it is important to do a quality presentation of characteristics that describe these basins of attraction, relations between aforementioned characteristics and possible implications of these results.

A scope of initial conditions $z_0 \in \bC_1$ is considered. 
These are the conditions that belong to a complex square defined as $\bC_1 = [-1,1]^2$.
This set includes all attractors for the default NDDS polynomial $p(z)=z^3-1$ and otherwise suits the needs of this work.
Within the chosen scope values which are $|\Dez| = 0.0025$ distance apart are evaluated, for example $-1, -0.9975, -0.995, \ldots , 1$.
It is easy to see that after aforementioned discretization a $801 \times 801$-sized grid of possible values for $z_0$ is obtained.

Similarly, a scope of parameters $\alpha  \in \bC_1$ is considered. 
It is enough for the experiments because outside of this set the behavior of NDDS can be quite extreme and too rapid-changing for the delicate numerical analysis.
Within the chosen scope some values which are $|\Delta \alpha| = 0.01$ distance apart are chosen, for example $-i, -0.99i, -0.98i, \ldots , i$.
Once again, it is easy to see that after discretization a $201 \times 201$-sized grid of possible values for $\alpha$ is obtained.

Lastly, the number of iterations used in the model is $n=30$. 
It corresponds to the depth of the trajectories $(z_0,\ z_1,\ z_2,\ \ldots,\ z_{30})$ and after $z_{30}$ the orbit is assigned to the nearest attractor $\cA$.
It is possible to use a higher depth but the orbits tend to stabilize before $n$ recalculations.
Also the number of elements in the grid is limited anyway.
Therefore, the benefits of significantly higher depth are questionable in the present setting.

\subsection[Fractal dimension]{Fractal dimension of basin boundaries}
For the purposes of research the algorithm discussed in subsection \ref{sec:algD} is used.
In the process of approximation, different coverings ranging from $1 \times 1$ to $100 \times 100$ pixels are taken into account.
This means $|S|=100$ cases overall.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basin-55_060i}
		\caption{$\alpha = -0.55+0.6i;\ D(\cF_\alpha)=1.21;$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basin050_090i}
		\caption{$\alpha = 0.5+0.9i;\ D(\cF_\alpha)=1.63.$}
	\end{subfigure}
	\caption[Basins of attraction with very different values of fractal dimension.]
	{Basins of attraction for the relaxed NDDS with varying values of fractal dimension. 
		Two different system parameters $\alpha \in \bC$ are used.}
	\label{fig:basins_D}
\end{figure}

In general, a fractal dimension $D$ is a characteristic of complexity comparing how detail in a pattern changes with the scale at which it is being measured.
For smooth simple curves this characteristic is a constant unit $D=1$ meaning that there is no change in detail. But other possibilities exist.
For example, let's take a particular parameter $\alpha = -0.55+0.6i$. 
Using the relaxed NDDS, its basins of attraction $\cB$, basin boundary $\cBo = \cF$ and finally the corresponding fractal dimension $D(\cF_\alpha)=1.21$ is obtained. 
This result is quite different from, say, $\alpha = 0.5+0.9i$ and corresponding dimension $D(\cF_\alpha)=1.63$.
The comparison can be seen visually in figure~\ref{fig:basins_D}.

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{\textwidth}
	\includegraphics{../img/sectionRealD}
	\caption{$\alpha \in [0,1]$;}
\end{subfigure}
\begin{subfigure}[b]{\textwidth}
	\includegraphics{../img/sectionImagD}
	\caption{$\alpha \in [0,1]i$.}
\end{subfigure}
\caption{Fractal dimensions for the relaxed NDDS with varying parameter $\alpha \in \bC$.}
\label{fig:sectionD}
\end{figure}

As the parameter $\alpha$ fluctuates starting from $\alpha = 0$, the changes in global characteristics are inspected.
At this stage the aim of the procedure is to detect any possible peculiarities.

Initially some small positive constant $\varepsilon \approx 0.001$ is added so that $\alpha \in [0,1]$.
Beginning from $D(\cF_0) \approx 1$ the fractal dimension increases almost monotonically until $D(\cF_1) \approx 1.38$ and onwards (for example, $D(\cF_2) \approx 1.60$).
This effect can be clearly seen in figure~\ref{fig:sectionD} part (a).

Next, starting from the same $\alpha = 0$, some small imaginary constant $\varepsilon \approx 0.001i$ is added so that $\alpha \in [0,1]i$.
Effectively this creates a section of the parameter plane along the imaginary axis.
But this time the dimension does not increase monotonically at all, see figure~\ref{fig:sectionD} part (b).
Instead it demonstrates periodic-like behavior and has some local minima approximately at $\alpha \in \{ \, 0.2i, 0.4i, 0.6i, 0.8i \, \}$.
This is indeed a peculiar behavior that requires further investigation.

\begin{figure}[ht]
\centering
\includegraphics{../img/planeD}
\caption{Fractal dimension $D(\cF_\alpha)$ in the parameter plane $\alpha \in \bC$.}
\label{fig:planeD}
\end{figure}

Then some computationally-intensive calculations are performed for many more possible $\alpha$ values.
As a result, fractal dimensions for the whole grid of parameter values is obtained.
This result is presented in figure~\ref{fig:planeD}.
It shows that the fractal dimension $D$ is a highly non-trivial function of complex parameter $\alpha \in \bC$ which apparently is symmetric with respect to the $\Im (\alpha) = 0$.

For $\Re (\alpha) \le 0$, the dimensions tend to be relatively low, with the maximum being found to be $D(\cF_{0.45i}) \approx 1.39$.
The periodic-like behavior along imaginary axis is also present only in this semi-plane.
Otherwise the imaginary part of the parameter $\alpha$ does not impact the observed dimension dramatically.

On the other hand, let's look at $\Re (\alpha) \ge 0$. In this semi-plane the dimensions vary substantially between 
$D(\cF_0) \approx 1$ and $D(\cF_{0.76 + 0.98i}) \approx 1.68$.
The periodic-like behavior is not to be found here.
As the $\Im (\alpha)$ approaches either $\pm 1$, the observed dimension increases dramatically.



\subsection[Wada measure]{Wada measure of basin boundaries}
The algorithm discussed in subsection \ref{sec:algW} is used for the following part of this research.
In the process of approximating Wada measure different coverings ranging from $2 \times 2$ to $101 \times 101$ pixels are taken into account (the single pixel case is trivial).
This means $|S|=100$ cases overall.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basin-85_053i}
		\caption{$\alpha = -0.85+0.53i;\ W(\cF_\alpha)=0.28;$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics{../img/basin010_060i}
		\caption{$\alpha = 0.1+0.6i;\ W(\cF_\alpha)=0.88.$}
	\end{subfigure}
	\caption[Basins of attraction with very different values of Wada measure.]
	{Basins of attraction for the relaxed NDDS with varying values of Wada measure. 
		Different parameters $\alpha \in \bC$ are used.}
	\label{fig:basins_W}
\end{figure}

In general, Wada measure $W$ indicates what percentage of boundary points $\cF$ is neighboring more than two basins of attraction simultaneously.
Naturally $W=0$ corresponds to $0\%$ while $W=1$ corresponds to $100\%$.

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{\textwidth}
	\includegraphics{../img/sectionRealW}
	\caption{$\alpha \in [0,1]$;}
\end{subfigure}
\begin{subfigure}[b]{\textwidth}
	\includegraphics{../img/sectionImagW}
	\caption{$\alpha \in [0,1]i$.}
\end{subfigure}
\caption{Wada measures for the relaxed NDDS with varying parameter $\alpha \in \bC$.}
\label{fig:sectionW}
\end{figure}

For example, let's take a particular parameter $\alpha = -0.85+0.53i$. 
Using Newton's discrete dynamical system its basins of attraction $\cB$, basin boundary $\cBo = \cF$ and finally the corresponding Wada measure $W(\cF_\alpha)=0.28$ is obtained. 
This result is very different from, say, $\alpha = 0.1+0.6i$ and corresponding measure $W(\cF_\alpha)=0.88$.
The comparison is presented visually in figure~\ref{fig:basins_D}.

As the parameter $\alpha$ varies in some direction starting from $\alpha = 0$, the changes in Wada measure are inspected.
Some resemblance to fractal dimension is expected.
However, it's worth emphasizing that Wada measure takes into account more data (basins of attraction themselves) compared to fractal dimension (only basin boundaries).

Initially some reasonably small positive constant $\varepsilon \approx 0.001$ is added so that $\alpha \in [0,1]$.
Beginning from $W(\cF_0) \approx 0$ the Wada measure then increases rapidly until approximately $\alpha \approx 0.03$ when $W(\cF_{0.03}) \approx 0.91$.
Afterwards it stays at a similar level until the end of simulations when $W(\cF_1) \approx 0.96$.
This effect can be clearly seen in figure~\ref{fig:sectionW} part (a).

In another scenario, let's add some small imaginary constant $\varepsilon \approx 0.001i$ so that $\alpha \in [0,1]i$.
This creates a section of the parameter plane along the imaginary axis.
This time the measure behaves in a very similar manner to fractal dimension, see figure~\ref{fig:sectionW} part (b).
Namely it demonstrates periodic-like behavior and has some local minima approximately at $\alpha \in \{ \, 0.2i, 0.4i, 0.6i, 0.8i \, \}$.

Overall this shows an expected resemblance compared to fractal dimension in one case (imaginary values), yet yields some new results in another scenario (real parameters).

\begin{figure}[ht]
\centering
\includegraphics{../img/planeW} 
\caption{Wada measure $W(\cF_\alpha)$ in the parameter plane $\alpha \in \bC$.}
\label{fig:planeW}
\end{figure}

Once again some computationally-intensive calculations for many more possible $\alpha$ values are performed.
As a result, Wada measure for the whole grid of parameter values $\alpha \in \bC_1$ is obtained.
This obtained result is presented in figure~\ref{fig:planeW}.
It shows that the Wada measure $W$ is indeed a non-trivial function of complex parameter $\alpha \in \bC$.
It is also apparently the case that the characteristic $W(\cF_\alpha)$ is invariant under $\alpha = x \pm iy$.

For $\Re (\alpha) \le 0$, the Wada measures vary significantly within $W \in [0.03, 0.90]$.
The average measure in this case is $\overline{W} \approx 0.53$.
The periodic-like behavior along imaginary axis is present only in this semi-plane.
This time the imaginary part of the parameter $\alpha$ can impact the observed measure noticeably.

On the other hand, let's look at $\Re (\alpha) \ge 0$. 
In this semi-plane the measure is relatively high.
The highest observed value is $W(\cF_{0.59 - i}) \approx 0.99$.
In fact the values are high almost everywhere except for $\Re (\alpha) \approx 0$ with average being $\overline{W} \approx 0.94$.
Also the periodic-like behavior is not found and $\Im (\alpha)$ has no substantial impact on the observed Wada measures.



\subsection[Relation between characteristics]{Relation between characteristics of basin boundaries}
The results concerning fractal dimensions and Wada measures have been described separately.
It was presented how and when these characteristics change within the family of basin boundaries.
The logical follow-up is to integrate the two characteristics and present the joint results regarding the relation between both measures.

\begin{figure}[ht]
\centering
\includegraphics{../img/scatterDW}
\caption{Fractal dimensions $D(\cF)$ and Wada measures $W(\cF)$ for all parameters $\alpha \in \bC_1$.}
\label{fig:scatterDW}
\end{figure}

Initially the scatter points are marked in figure~\ref{fig:scatterDW} to plot the set of observations $D$ and $W$ for the data.
Some tendencies can be noticed right away.
Apparently there exists a positive correlation between the characteristics: the higher the fractal dimension $D$, the higher the Wada measure $W$.
Different coefficients may be used to evaluate possible correlation:
\begin{enumerate}
\item Pearson's correlation coefficient measures the linear relationship between two continuous variables. 
The normality is not assumed nor checked. In fact only finite (co)variances are assumed. 
The obtained outcome is
\begin{equation}
\rho_{D,W}= \frac{\overline{DW} - \overline{D} \cdot \overline{W}}{\sqrt{\overline{D^2} - \overline{D}^2} \sqrt{\overline{W^2} - \overline{W}^2}} 
= 0.82
\end{equation}
which indeed reveals a positive correlation.

\item Spearman's correlation coefficient measures the monotonic relationship between two continuous variables. 
The outcome is calculated very similarly, yet the measures themselves are replaced with ranks
\begin{equation}
r_{D,W} = \rho_{\rank(D),\rank(W)} = 0.87
\end{equation}
and once again a positive correlation is revealed.
Because of ranking feature, the measure is relatively robust to outliers (unlike Pearson's correlation).
It is possible that there exist some numerical outliers since the nature of the calculations is quite delicate.
\end{enumerate}

Despite some positive correlation the relationship is still complex.
It is possible to provide a sequence of fractal basin boundaries whose dimension $D$ strictly increases, yet at the same time the corresponding Wada measure decreases.
The aforementioned effect can be demonstrated in the scatter plot (see figure~\ref{fig:scatterDW}) by selecting a negatively correlated subset of observations ($\rho \approx -1$).

\begin{figure}[ht]
\centering
\includegraphics{../img/cumulativeCW}
\caption[Minimum and maximum fractal dimensions for a fixed Wada measure]
{Minimum and maximum fractal dimensions for the $\varepsilon$-neighborhoods of a fixed Wada measure $W(\cF)$.}
\label{fig:cumulativeC}
\end{figure}

Now in figure~\ref{fig:cumulativeC} it is checked what fractal dimensions $D$ exist when a particular Wada measure $W$ is fixed.
It is actually possible to find relatively low fractal dimensions $D < 1.2$ for (almost) all Wada measures $W < 0.9$.
Afterwards when $W \ge 0.9$ the minimal dimension $D$ is necessarily higher within the scope of parameters $1.2 \le D < 1.6$.
On the other hand, the maximal dimension increases along with fixed Wada measure with few exceptions in the present setting.

These observations can also be presented the other way around.
For example, once the complex patterns have $D > 1.2$, a substantial Wada measure is expected as well - otherwise there would be no matches.



\section{Implications of chaos} \label{sec:I}
\subsection{Uncertainty of the final attractor}
In order to check the underlying uncertainty of the final attractor the setting of the NDDS is fixed as described below.
The initial seeds belong to a grid of values around $z_0 = -0.793$ within the radius of $|\Dez_0| = 0.01$.
To ensure that there are enough simulations the distance between nodes in the grid has to be sufficiently small, around $\varepsilon \approx 0.0006$.
This leads to the total number of simulations being $s = 795$ which is considered to be enough for reliable results.

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{0.32\textwidth}
	\includegraphics{../img/uncertainty097_000i}
	\caption{$\alpha = 0.97;$}
\end{subfigure}
\begin{subfigure}[b]{0.32\textwidth}
	\includegraphics{../img/uncertainty100_000i}
	\caption{$\alpha = 1;$}
\end{subfigure}
\begin{subfigure}[b]{0.32\textwidth}
	\includegraphics{../img/uncertainty100_005i}
	\caption{$\alpha = 1 + 0.005i.$}
\end{subfigure}
\caption[Uncertaintly of the final attractor]
{Uncertainty of the final attractor when the initial seed $z_0 = -0.793$ fluctuates within $|\Dez_0| = 0.01$ and the parameter $\alpha$ is slightly perturbed.}
\label{fig:uncertainty}
\end{figure}

Initially the control parameter $\alpha = 0.97$ is set.
It leads to the emergence of slightly smoothed basin boundary.
After running the simulations, it is noticeable that the outcomes are quite tendentious: $523$ cases converge to the attractor $\cA_3$ while the other two $\cA_1$ and $\cA_2$ attract $s(\cA_1) = s(\cA_2) = 136$ precisely.
The radar plot can be seen in figure~\ref{fig:uncertainty}(a).

Next the control parameter $\alpha = 1$ is set.
It leads to the emergence of a standard basin boundary of the NDDS.
Simulations reveal that the likelihood of outcomes is almost even now: $s(\cA_3) = 259$ cases go to the attractor $\cA_3$ while $268$ cases are attracted by $\cA_1$ and $\cA_2$ each.
The result is shown in figure~\ref{fig:uncertainty}(b).

Finally, the control parameter $\alpha = 1 + 0.005i$ is chosen.
It leads to the emergence of a rotated boundary.
After inspecting the outcomes, the change in situation is seen: attractor $\cA_2$ is now dominant with $s(\cA_2) = 472$ cases while $\cA_1$ and $\cA_3$ attract $261$ and $62$ different initial seeds respectively.
The visual outcome is shown in figure~\ref{fig:uncertainty}(c).

\begin{table}[h]
\centering
\begin{tabular}{| l | c | c | c |}
	\hline			
	$\alpha$ & $s(\cA_1)$ & $s(\cA_2)$ & $s(\cA_3)$ \\
	\hline \hline
	$0.97$ & $136$ & $136$ & $523$ \\ \hline
	$1$ & $268$ & $268$ & $259$ \\ \hline
	$1+0.005i$ & $261$ & $472$ & $62$ \\ \hline
\end{tabular}
\caption{Distribution of simulations (out of $s=795$) that go to different attractors $\cA_1,\ \cA_2,\ \cA_3$ under various values of parameter $\alpha$.}
\label{tab:sim}
\end{table}

The overall resulting situation is reviewed in table~\ref{tab:sim} and presented visually in figure~\ref{fig:uncertainty}.
To sum up, some complicated interactions between the resulting attractors are observed during simulations. 
Moreover, these interactions involve more than two attractors simultaneously.
This means that during research it is crucial to consider not only the boundary itself (in terms of fractal dimension or equivalent characteristic) but also the neighboring basins of attraction (in terms of Wada measure or some other means).



\subsection{Controlling the dynamical system}
In some cases it is important to stabilize and control the nonlinear dynamical system as required.
It is not always an easy task. 
Some particular initial seeds are chosen
$z_0 \in \{ \, -0.793-0.001i, -0.793, -0.793+0.001i \, \}$, 
then the evolution of dynamics is observed while the control parameter $\alpha = 1$ is slightly perturbed until $\alpha = 0.99$.

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{\textwidth}
	\includegraphics{../img/trajectories100_000i}
	\caption{$\alpha = 1$; \\ \quad}
\end{subfigure}
\begin{subfigure}[b]{\textwidth}
	\includegraphics{../img/trajectories099_000i}
	\caption{$\alpha = 0.99$.}
\end{subfigure}
\caption[Trajectories of the NDDS orbits for the perturbed parameter $\alpha \in \bC$]
{Trajectories of the Nn orbits when the initial seed $z_0 = -0.793$ varies by $\Delta z = 0.001i$ and the parameter $\alpha$ is slightly perturbed.}
\label{fig:trajectories}
\end{figure}

It is not possible to plot complex orbits in less than three dimensions so other acceptable ways of representing the situation are considered.
Because of fluctuations in magnitude that occur while observing orbits the complex arguments (see figure~\ref{fig:trajectories}) are plotted.
Understandably the convergence of arguments does not ensure the convergence of values hence this aspect is checked separately.

Then $\alpha = 1$ is set along with aforementioned initial seeds which have the property $\Arg(z_0) \approx \pi$. 
As the orbits evolve the trajectories diverge briefly, approximately until iteration $n=4$ and begin to stabilize afterwards.
By the time the iteration $n=35$ is reached, the arguments are stable and so are the values.
In fact the values have converged to three distinct attractors $\cA_1,\ \cA_2,\ \cA_3$.
The values are shown in figure~\ref{fig:trajectories}(a).

In another case the control parameter is perturbed to $\alpha = 0.99$.
It is a relatively slight shift yet the effect is critical.
All initial seeds remain the same and the property $\Arg(z_0) \approx \pi$ does not change during iterations.
In fact the values continue to remain in the starting region during the whole experimental period $n = \ival{0}{35}$. 
These orbits are presented in figure~\ref{fig:trajectories}(b).

To sum up the trajectories of orbits depend on the control parameter $\alpha$.
In case of more than two attractors the final outcomes are attracted by multiple distinct regions.
Therefore, the ability to predict convergence of an arbitrary initial seed is influenced not only by the positioning and complexity of the basin boundary but also by the intertwining of the basins themselves.
While the formal aspect can be somewhat measured using fractal dimension, the latter requires Wada measure or equivalent tools.



\section{Discussion}
Newton's discrete dynamical system is analyzed numerically in terms of basins of attraction $\cB$ and their fractal boundaries $\cBo = \cF$, using control parameter $\alpha \in \bC$.

Fractal dimension $D$ is evaluated using the box counting technique. 
Some peculiar dynamical processes are noticed, especially when the imaginary part of $\alpha$ varies, revealing periodic-like behavior.
The dimension itself fluctuates from $D = 1$ in a trivial case, to $D = 1.68$ for a very complicated structure.

Wada measure $W$ is calculated using a novel proposed algorithm. 
The results only somewhat resemble those concerning fractal dimension. 
Most notably the proposed Wada characteristic also takes into account the basins of attraction.
The measure itself varies from $W = 0.03$ in a trivial case, to $W = 0.99$ for especially intertwined boundaries.

The relation between characteristics $D$ and $W$ is highly non-trivial. 
There is a positive correlation but nothing like a bijective correspondence can be defined.
This is understandable since Wada measure takes into account more data.

The uncertainty of the final attractor $\cA$ is examined.
It can be an extremely complicated task to determine when (and where) a specific trajectory converges.
This difficulty to predict the outcome can be evaluated quantitatively in terms of both characteristics.

Unlike other research, the limited precision is emphasized in this work. 
It means that once the grid of value simulations is obtained, it is not possible to zoom in order to obtain an even more detailed picture of the model.
This apparent drawback can also be seen as a realistic situation when only some experimental observations are available instead of the precise model.