## Chaotic processes and their applications in complex dynamical systems

#### Authorship

Author: Pranas Ziaukas

Supervisor: prof. habil. dr. Minvydas Ragulskis. 

The Faculty of Mathematics
and Natural Sciences, Kaunas University of Technology.

#### Metadata

Research area and field
* physical sciences
* mathematics

Key words 
* dynamical systems
* chaos
* attractors
* fractal dimension
* Wada property

Kaunas, 2016. 50 p.

#### Summary 

Chaotic processes are analyzed in this thesis using multiple techniques. A relaxed Newton’s
discrete dynamical system with a complex control parameter is chosen and simulated numerically.
Different set-ups of attracting basins are obtained along with their boundaries. The fractal dimensions
of basin boundaries are measured using the box counting algorithm. The Wada qualities of
basins are evaluated using a novel proposed algorithm. Both characteristics are compared in a
two-dimensional parameter plane, non-trivial relations between them are emphasized. Examples
of a complex transient behavior under certain conditions are provided. Finally, advantages and
disadvantages of a proposed Wada characteristic are discussed, and recommendations are made
regarding further research.
