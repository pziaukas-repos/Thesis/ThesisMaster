clear;
load('../calc/NewtonWD.mat');
addpath('../code');

for k = 1:2;
switch k;
case 1;
    str = '5';
    poly = [1 0 0 0 0 -1];
    fZ = Newton(Z, n, 1, poly);
    A = NewtonAttractors(fZ, poly);
case 2;
    str = '2';
    poly = [1 0 0 -1 0];
    fZ = Newton(Z, n, 1, poly);
    A = NewtonAttractors(fZ, poly);
end;
    
cmap = [.9 .1 .1; .9 .9 .1; .1 .1 .9; 0 0 0; .1 .9 .1];
imshow(label2rgb(A, cmap, 'k'), 'XData', xMin:d:xMax, 'YData', yMin:d:yMax);
daspect('auto');
% xlabel('$\Re(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
% ylabel('$\Im(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.14 0.1 0.85 0.85],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') str '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 7 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 7 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);


end;