clear;
load('../calc/NewtonWD.mat');
addpath('../code');

fZ = zeros([size(Z) n]);
fZ(:,:,1) = Newton(Z);
for k = 2:n;
    fZ(:,:,k) = Newton(fZ(:,:,k-1));
end;
M = HankMap(fZ);

imshow(uint8(255*mat2gray(M)), 'XData', xMin:d:xMax, 'YData', yMin:d:yMax);
colormap jet;
% cbar = colorbar('YTickLabel', {round(100 * (min(M(:)):.2*(max(M(:))-min(M(:))):max(M(:))))/100});
cbar = colorbar('YTickLabel', {1, 4, 7, 10, 13, 16});
daspect('auto');
xlabel('$\Re(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
ylabel('$\Im(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.11 0.1 0.75 0.87],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 15 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 13 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);
