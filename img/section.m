clear;
load('../calc/NewtonWD.mat');
zoom = 3;

for k = 1:4;
switch k;
    case 1;
        str = 'ImagD';
        M = flipud(D(any((P == 0) .* (Q >= 0), 3)));
        plot(0:h/zoom:qMax, spline(0:h:qMax, M, 0:h/zoom:qMax), 'k', 'LineWidth', 2);
        xlabel('$\Im(\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$D(\mathcal{F}_\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
    case 2;
        str = 'RealD';
        M = D(any((P >= 0) .* (Q == 0), 3));
        plot(0:h/zoom:qMax, spline(0:h:qMax, M, 0:h/zoom:qMax), 'k', 'LineWidth', 2);
        xlabel('$\Re(\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$D(\mathcal{F}_\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
    case 3;
        str = 'ImagW';
        M = flipud(W(any((P == 0) .* (Q >= 0), 3)));
        plot(0:h/zoom:qMax, spline(0:h:qMax, M, 0:h/zoom:qMax), 'k', 'LineWidth', 2);
        xlabel('$\Im(\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$W(\mathcal{F}_\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
    case 4;
        str = 'RealW';
        M = W(any((P >= 0) .* (Q == 0), 3));
        plot(0:h/zoom:qMax, spline(0:h:qMax, M, 0:h/zoom:qMax), 'k', 'LineWidth', 2);
        xlabel('$\Re(\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$W(\mathcal{F}_\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
end;

daspect('auto');
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.1 0.18 0.88 0.75],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') str '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 15 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 3 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;