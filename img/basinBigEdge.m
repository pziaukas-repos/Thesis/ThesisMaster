clear;
load('../calc/NewtonWD.mat');
addpath('../code');

for r = 1;
fZ = Newton(Z, n, r);
A = NewtonAttractors(fZ);
F = edge(A);
imshow(1-F, 'XData', xMin:d:xMax, 'YData', yMin:d:yMax);
daspect('auto');
xlabel('$\Re(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
ylabel('$\Im(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.14 0.12 0.85 0.85],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') sprintf('%03d_%03di', round(100*real(r)), round(100*imag(r))) '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 12 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 12 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;