clear;
addpath(genpath('../'));

% Hard parameters
xMin = -1; xMax = 1;
yMin = -1; yMax = 1;
d = 0.0025;
n = 30;

[X, Y] = meshgrid(xMin:d:xMax, yMax:-d:yMin);
Z = X + 1i * Y;

r = 0.5 + 0.5*1i;

fZ = Newton(Z, n, r);
A = Attractors(fZ);
subplot(1,2,1);
Paint(A);
fZ = Newton(Z, n, conj(r));
A = Attractors(fZ);
subplot(1,2,2);
Paint(A);