clear;
load('../calc/NewtonWD.mat');
addpath('../code');

for r = [0 0.5 1 1.5 2 0.45*1i 0.54*1i 0.62*1i 1i 1+0.5*1i 1-0.5*1i 0.5+0.9*1i 0.1+0.6*1i -.55+.6*1i -.85+.5325*1i];

fZ = Newton(Z, n, r);
A = NewtonAttractors(fZ);
cmap = [.9 .1 .1; .9 .9 .1; .1 .1 .9; 1 1 1];
imshow(label2rgb(A, cmap, 'k'), 'XData', xMin:d:xMax, 'YData', yMin:d:yMax);
daspect('auto');
% xlabel('$\Re(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
% ylabel('$\Im(z_0)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.14 0.1 0.85 0.85],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') sprintf('%03d_%03di', round(100*real(r)), round(100*imag(r))) '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 7 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 7 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;