function wada

h = 100;
w = 1000;

cmap = [.9 .1 .1; .9 .9 .1; .1 .1 .9];
M = zeros(h, w);

step = 0;
d0 = round((1+w)*[1 2]/3);
M(:, 1:d0(1)) = 1;
M(:, d0(2):w) = 2;
Print;

step = step+1;
d1 = Touch(d0(1), d0(2));
Print;

step = step+1;
d2(1,:) = Touch(d0(1), d1(1));
d2(2,:) = Touch(d1(2), d0(2));
Print;

step = step+1;
d3(1,:) = Touch(d0(1), d2(1,1));
d3(2,:) = Touch(d2(1,2), d1(1));
d3(3,:) = Touch(d1(2), d2(2,1));
d3(4,:) = Touch(d2(2,2), d0(2));
Print;

function d = Touch(a, b)
    c = 6 - (M(1,a-1) + M(1,b+1));
    s = round((b-a)/3);
    d = [(a+s) (a+2*s)];
    M(:, d(1):d(2)) = c;
end

function Print
    imshow(label2rgb(M, cmap, 'k'));
    fname = [mfilename('fullpath') sprintf('%d', step) '.png'];
    figureW = floor(96 * w/100 / 2.54); % screenDPI * widthCM / inchCM
    figureH = floor(96 * h/100 / 2.54);
    set(gcf, 'Position', [50 50 figureW figureH]);
    set(gcf, 'PaperPositionMode', 'auto');
    print(gcf, '-r300', '-dpng', fname);
end

end