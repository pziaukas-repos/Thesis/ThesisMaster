function PaintRadar( A, varargin )
% Radar plot of attractors
% PaintRadar(A, [isoC, P])
% of basin points A using isoC isocircles
% for the polynomial P

optargs = {-1, [1 0 0 -1]};
optargs(1:nargin-1) = varargin;
[isoC, P] = optargs{:};

s = 10^floor(log10(numel(A)));
R = roots(P)';
sR = length(R);

C = zeros(1,sR);
for k = 1:sR;
    C(k) = sum(A(:) == k);
end;
if (isoC == -1);
    isoC = ceil(max(C)/s);
end;
isoCr = linspace(0, .9, isoC+1);
isoCr(1) = [];

hold on;
plot([zeros(1,sR); real(R)], [zeros(1,sR); imag(R)], 'r', 'LineWidth', 2);
plot(real(R), imag(R), 'ro', 'MarkerFaceColor','r');
viscircles(repmat([0 0], isoC, 1), isoCr, 'LineWidth', 1);
for k = 1:isoC;
    text(isoCr(k)*cos(-pi/4)-.05, isoCr(k)*sin(-pi/4), sprintf('%d', k*s), 'FontSize',10);
end;
for k = 1:sR;
    text(real(R(k))+.05, imag(R(k)), sprintf('A_%d', k), 'FontSize',12);
end;

fC = R .* C * .9 / (isoC*s);
fC = [fC fC(1)];
plot(real(fC), imag(fC), 'k', 'LineWidth', 3);

axis([-1.1 1.1 -1.1 1.1]);
axis square;
set(gca,'visible','off');

end