function d = Dimension( A, varargin )
% Fractal dimension
% of the boundary F
% for the basins of attraction A
% and counting depth h

optargs = {100};
optargs(1:nargin-1) = varargin;
[h] = optargs{:};

F = edge(A);

x = -log(1:h);
for k = h:-1:1;
    y(k) = log(BCount(k));
end;

% Simple linear regression
L = [ones(1,h); x]'\y'; 
d = max(1, L(2));


function y = BCount(k)
% Box-count
% of k-by-k boxes
% for fractal F

M = im2col(F, [k k], 'distinct');
y = sum(sum(M,1)>0);
    
end

end

