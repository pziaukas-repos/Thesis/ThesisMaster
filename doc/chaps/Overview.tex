\chapter{Overview}
In a relatively short and concise overview the necessary theoretical preliminaries and widely accepted concepts used in this work are described.

Section \ref{sec:C} is dedicated to the realm of complex numbers and complex analysis, section \ref{sec:DS} introduces discrete dynamical systems and, lastly, section \ref{sec:F} presents some general ideas regarding self-similarity and fractals.



\section{Complex analysis} \label{sec:C}
Initially, some real numbers $x,y \in \bR$ are considered.
Under the well-known operations of addition "$+$", subtraction "$-$", multiplication "$\cdot$" or division "$\div$" the resulting quantities remain real.
However, in case of square roots (such as $\sqrt{x}$), or solutions to polynomial equations with real coefficients (such as $x^2 = -1$) the property of being real does not hold anymore. 
It has been accepted that sometimes there are no solutions to the proposed problems amongst real numbers $\bR$ (see \cite{Burton2007}).
An alternative is to use a more general realm - like the set of complex numbers $\bC$.



\subsection{Complex numbers}
The earliest references to the concept of complex numbers are attributed to the mathematicians of the ancient Greece~\cite{Burton1995}.
The notion of an imaginary unit $\sqrt{-1}$ was introduced in the sixteenth century, and later became (as discussed in~\cite{Nahin2010}) simplified as $i$.
Over years the idea matured and evolved into the modern notion of complex numbers that is known nowadays.

\begin{mydef}
A {\bfseries complex number} is a pair $z = x+iy$, where the components $x,y \in \bR$ and $i$ is an imaginary unit satisfying $i^2 = -1$.
The set of all complex numbers is denoted $\bC$. $\qed$
\end{mydef}
\noindent
Following the introduced notation, for any complex number $z = x+iy \in \bC$ there is two parts:
\begin{itemize}
	\item {\bfseries real part} $\Re(z)=x$, that does not have an imaginary unit;
	\item {\bfseries imaginary part} $\Im(z) = y$, that, on the contrary, is matched with an imaginary unit.
\end{itemize}
All the arithmetical operations are accomplished in an intuitive way, as in $\bR$. 
Real and imaginary parts are treated as separate summands.
\begin{myex}
	The multiplication of two complex numbers $z_1$ and $z_2$ is accomplished as follows
	\begin{multline*}
	z_1 \cdot z_2 = (x_1 + i y_1) \cdot (x_2 + i y_2) = x_1 x_2 + i x_1 y_2 + i y_1 x_2 + i^2 y_1 y_2 = \\ =
	x_1 x_2 + i x_1 y_2 + i y_1 x_2 - y_1 y_2 = (x_1 x_2 - y_1 y_2) + i(x_1 y_2 + y_1 x_2).
	\end{multline*}
\end{myex}




\subsection[Geometrical interpretation]{Geometrical interpretation of complex numbers}
Typically real and imaginary parts (say $x$ and $y$) of a given complex number $z \in \bC$ are interpreted as components of some vector $(x , y) \in \bR^2$ in a Euclidean plane.
It is a consequence of the fact that there exists a bijective mapping between $\bC$ and $\bR^2$. 
In other words, every complex number $z$ may be assigned a vector $(x , y)$ as its visual counterpart, and vice versa.
It is known as a geometrical representation of a given complex number~\cite{Burton1995}.
The idea is illustrated in figure~\ref{fig:geocomplex}.
\begin{figure}[ht]
	\centering
	\includegraphics{../img/geocomplex}
	\caption[Geometrical interpretation of a complex number]
	{Geometrical interpretation of a complex number.
		This includes real and imaginary parts $\Re(z)$ and $\Im(z)$ respectively, complex modulus $|z|$, and also argument $\varphi(z).$}
	\label{fig:geocomplex}
\end{figure}

In terms of classical geometry, a two-dimensional vector may be characterized by its length, angle with respect to some axis, and other properties.
Since there exists a direct correspondence between Euclidean $\bR^2$ and complex $\bC$ planes, the very same idea applies to complex numbers.
Therefore some more relevant definitions regarding complex numbers are presented below.
\begin{mydef} \label{def:modulus}
	The {\bfseries modulus} (or the {\bfseries norm}) of a complex number $z = x+iy$ is denoted and defined as $|z| = \sqrt{x^2+y^2}$. $\qed$
\end{mydef}
It should be added that generally norm is a function that maps vectors to positive numbers (except the origin vector which is mapped to zero) and satisfies the properties of scalability, subadditivity and separability~\cite{Friedman1970}.
Thus there can be different norms that are equally acceptable in a particular field of mathematics. 
However, in this work the classical context is used, and the traditional case of complex modulus is presented. 

\begin{mydef}
	The {\bfseries argument} of a complex number $z = x+iy$ is the angle $\varphi \in [0;2\pi)$ between the vector $(x , y)$ and the real positive semi-axis $\bR^{+}$. It is denoted $\Arg(z)$. $\qed$
\end{mydef}
Once again, there are different interpretations of this characteristic.
For example, the values of an angle $\varphi$ can be chosen from $[-\pi;\pi)$ or, for the sake or argument, even $[-2\pi;0)$.
This would not go against mathematical requirements. 
In this work the interval $[0;2\pi)$ is selected.



\subsection[Differentiation]{Differentiation of complex functions}
In case of real numbers, the idea of differentiation arises from the need to compute instantaneous velocities or slopes of tangent lines.
In case of complex numbers, the differentiation is not so intuitive. 
However, in both cases it is a matter of computing some limit values.
\begin{mydef}
	Function $f(z)$ is {\bfseries differentiable} at a point $z_0 \in \bC$ if it is defined in the neighborhood of $z_0$ and the following limit exists
	\begin{equation}
	f'(z_0) = \lim_{z \to z_0} \frac{f(z) - f(z_0)}{z - z_0}.
	\end{equation}
	$f'(z_0)$ is then referred to as the {\bfseries derivative} of $f$ at $z_0$. $\qed$
\end{mydef}
One important aspect that is not present in the case of real numbers, is that the limit value has to be approached from infinitely many directions instead of just two.
Furthermore, it is not a straightforward procedure to interpret or visualize the outcome when the function $f$ is of complex nature. 
In some cases, it can be really challenging or even impossible~\cite{Lundmark2004}. 

\begin{myex}
	Let's compute the derivative of $f(z) = z^3$ for all $z \in \bC$,
	\begin{multline*}
	f'(z) = \lim_{\Dez \to 0}\frac{(z+ \Dez)^3 - z^3}{\Dez} =
	\lim_{\Dez \to 0}\frac{z^3 + 3z^2 \Dez +3z \Dez^2+ \Dez^3 - z^3}{\Dez} = \\ =
	\lim_{\Dez \to 0}\frac{3z^2 \Dez +3z \Dez^2+ \Dez^3}{\Dez} =
	%\lim_{\Dez \to 0}\frac{\Dez (3z^2 +3z \Dez+ \Dez^2)}{\Dez} = \\ =
	\lim_{\Dez \to 0} \left(3z^2 +3z \Dez+ \Dez^2\right) = 3z^2.
	\end{multline*}
\end{myex}




\subsection[Physical interpretation]{Physical interpretation of complexity}
The complex derivative cannot be interpreted as an instantaneous velocity or some steepness of a tangent slope.
No quantities can be measured nor counted as imaginary values in a usual tangible sense. 
Despite all that, complex numbers are quite useful for enhancing different types of mathematics, generalizing ideas, and extending the scope of real numbers~\cite{Nahin2010}.

\begin{itemize}
	\item The complex modulus is directly related to the notions of magnitude, distance, and absolute value in various physical contexts. 
	\item The argument represents rotational characteristics and positioning in space.
	\item The existence of complex derivative is a very strong condition which comes in pair with an opposite effect of integration.
\end{itemize}

As a consequence, in fluid dynamics complex valued functions are used to describe potential flows~\cite{Terentiev1997}.
Closely related disciplines such as aerodynamics, hydrodynamics, and hydraulics have a range of applications that require usage of two dimensional quantities as well~\cite{Milne-Thomson2012}.
In electrical engineering, the characteristics of resistors, capacitors, and inductors can be combined by introducing imaginary resistances~\cite{Oz2015}.
In special and general relativity, some formulas manipulate complex values, including the famous Schrödinger equation in quantum mechanics~\cite{Nelson1966}.

All this is part of the foundation of modern mathematics and mathematical physics that would not be possible without complex analysis.





\section{Discrete dynamical systems} \label{sec:DS}
During recent decades the discipline of nonlinear dynamics has grown significantly~\cite{Khalil2002}.
Introduction of deterministic chaos and nonlinearity have provided some new and innovative tools that allow scientific community to better assess and understand surprisingly complex behaviors of relatively simple systems.
The aim of this subsection is to provide some preliminaries that are necessary in order to proceed with the complete research of this work.



\subsection[Introduction]{Introduction to discrete dynamical systems}
In case of discrete-time dynamical systems, the dynamics are fully described by some iterated maps, also known as evolutionary functions~\cite{Thompson2002a}. 
\begin{mydef}
An {\bfseries iteration} of a discrete dynamical system is a function $f$ of the form
\begin{equation}
z_{n+1} = f(z_n);
\end{equation}
that takes and returns complex values $z_n, z_{n+1} \in \bC$. $\qed$
\end{mydef}
It is understandable that some further restrictions may be enforced on argument $z_n$.
The model may only be valid under certain circumstances or in specific situations.
However, these restrictions vary from system to system.

\begin{figure}[ht]
	\centering
	\includegraphics{../img/orbit}
	\caption[Orbit of a dynamical system]
	{Orbit of a given dynamical system. 
		Initial seed $z_0$ evolves into $f(z_0)=z_1$, then $f(z_1)=z_2$ and so on as required.}
	\label{fig:orbit}
\end{figure}

Another important notion is to realize that the evolution of $z_0$ may be immensely complex as the iterations proceed in time under the specified rule $f$.

\begin{mydef}
For a given dynamical system $f$, the {\bfseries orbit} of $z_0 \in \bC$ is the sequence of iterates
\begin{equation}
(z_0,\ z_1,\ z_2,\ \ldots) =
\begin{pmatrix}
z_0 & f(z_0) & f(f(z_0)) & \ldots
\end{pmatrix};
\end{equation}
also referred to as a trajectory of $z_0$. $\qed$
\end{mydef}

It is typical to denote the repeated applications of $f$ using the notation of superposition
\begin{equation}
	f^{\circ n} (z_0) = \underbrace{f(f(\ldots f(}_\text{$n$ times} z_0)\ldots ).
\end{equation}
One of the main objectives in the modern theory of dynamical systems is to fully understand the properties of orbits (see figure~\ref{fig:orbit})~\cite{Katok1997a}.

\begin{myex}
Let's look at the specific orbit of the discrete dynamical system described by the map $f(z) = 3z(1-z)$.
An initial seed $z_0 = 1/3$ is fixed and further investigated 
\begin{align*}
	&z_0 = 1/3,\\
	&z_1 = f(z_0) = f(1/3) = 3 \cdot 1/3 \cdot (1-1/3) = 2/3,\\ 
	&z_2 = f(z_1) = f(2/3) = 3 \cdot 2/3 \cdot (1-2/3) = 2/3, \quad \text{and generally} \\
	&z_n = f(z_{n-1}) = 2/3, \quad \text{whenever } n \in \bN.
\end{align*}
A precise orbit $(1/3,\ 2/3,\ 2/3,\ \ldots)$ which converges relatively quickly is obtained.
The topics of attractors and convergence are discussed in the following subsections.
\end{myex}



\subsection[Attractors]{Attractors of discrete dynamical systems}
Dynamical systems may possess one or several attractors.
Simply put, attractors are structures which initial conditions tend to evolve to.
Some more rigorous definitions are presented in~\cite{Strogatz1994, Katok1997a, Galor2007}.
One of possible approaches is formulated in this subsection.

\begin{mydef}
An {\bfseries attractor} is a set of points $\cA \subset \bC$ that is invariant under the dynamics, that is
\begin{equation}
\forall z_0 \in \cA \quad z_1 = f(z_0) \Rightarrow z_1 \in \cA;
\end{equation} 
and towards which some neighborhood $\cN \subset \bC$ evolutes in the course of time
\begin{equation}
\forall z_0 \in \cN \quad z_n = f(z_{n-1}) \Rightarrow \lim_{n \to +\infty} z_n \in \cA.
\end{equation}
It is also the smallest unit which cannot be divided into two or more attractors with distinct neighborhoods $\cN$. $\qed$
\end{mydef}

Additionally, an attractor satisfies the following conditions:
\begin{enumerate}
	\item $\cA$ is a bounded and closed set that is invariant under the dynamical system.
	\item $\cA$ attracts some open neighborhood of initial conditions.
	\item $\cA$ is strictly minimal and has no proper subset satisfying previous conditions.
	\item $\cA$ is stable in terms of Lyapunov stability. \\
	In simple terms, if the solutions around an attractor $\cA$ stay in that vicinity forever, then $\cA$ is Lyapunov stable.
\end{enumerate}

An attractive set $\cA$ has some peculiar properties.
Once the attracting neighborhood $\cN$ is determined, it follows that the distance from an arbitrary element $z_0 \in \cN$ to $\cA$ (in terms of a Euclidean metric) tends to diminish over time. 
This is a crucial concept. The largest such set is known as the basin of attraction.

\begin{mydef}
The largest set $\cN \subset \bC$ such that its members approach $\cA$ over time
\begin{equation}
\forall z_0 \in \cN \quad \lim_{n \to +\infty} d(z_n, \cA) = 0;
\end{equation}
is called the {\bfseries basin of attraction} $\cB(\cA)$ or simply $\cB$. $\qed$
\end{mydef}
It is quite common for discrete dynamical systems to have multiple attractors~\cite{Thompson2002a}. 
For each such attractor, there exists a corresponding basin of attraction which consists of distinct elements taken from the complex plane $\bC$.

\begin{myex}
Let's look at the attractors of the discrete dynamical system described by the map $f(z) = z/2$.
For an arbitrary initial seed $z_0 \in \bC$, it is evident that
\begin{equation*}
\lim_{n \to +\infty} f^{\circ n} (z_0) = \lim_{n \to +\infty} \left( z_0 / 2^n \right) = 0.
\end{equation*}
By definition, it can be concluded that the system has a single attractor $\cA = \{ \, 0 \,  \}$ 
with its basin of attraction being the whole complex plane $\cB = \bC$.
\end{myex}



\subsection[Chaotic properties]{Chaotic properties of discrete dynamical systems} \label{sec:DSprops}
There are many different met  hods to look at the chaotic properties of a given dynamical system~\cite{Devaney2003}.
One of the signs of chaos is the rapid divergence of nearby trajectories.
In fact, this divergence is sometimes even exponential in time (or with respect to iterations).
Similarly, there are various different tools proposed to effectively analyze chaos. 
These, for example, are Lyapunov exponents or $H$-ranks that are presented in this subsection. 

\subsubsection{Lyapunov exponents}
Say that an initial seed belongs to some attractor $z_0 \in \cA$.
Let's look at how $z_0$ differs from $z_0 + \varepsilon$ over time for some arbitrary difference $\varepsilon \in \bC$.
In other words, an iterated map $f$ is applied.
Then the absolute difference between the orbits (previously defined as complex modulus \ref{def:modulus}) is
\begin{equation}
d_n = |f^{\circ n} (z_0 + \varepsilon) - f^{\circ n} (z_0)|.
\end{equation}
In case that the behavior is chaotic, then the divergence should be exponential on $n$
\begin{equation}
\frac{d_n}{\varepsilon} = \frac{|f^{\circ n} (z_0 + \varepsilon) - f^{\circ n} (z_0)|}{\varepsilon} = e^{\lambda n}.
\end{equation}
After expressing power $\lambda$, the result is 
\begin{equation}
\lambda = \frac{1}{n} \log \left( \frac{|f^{\circ n} (z_0 + \varepsilon) - f^{\circ n} (z_0)|}{\varepsilon} \right).
\end{equation}
This defines what is meant by the speed of divergence. 
When $\varepsilon \to 0$ and the chain rule for differentiation is applied, then the strict definition of Lyapunov exponent is obtained.

\begin{mydef}
The {\bfseries Lyapunov exponent} around $z_0 \in \bC$ is defined by
\begin{equation}
\lambda = \frac{1}{n} \log \left( \prod_{k=0}^{n-1} |f'(z_k)| \right) =
\frac{1}{n} \sum_{k=0}^{n-1} \log \left( |f'(z_k)| \right).
\end{equation}
It is the rate of divergence between two neighboring trajectories. $\qed$
\end{mydef}

In the presented definition the Lyapunov exponent is calculated only at a single point $z_0 \in \bC$.
However, in the literature~\cite{Hilborn2000} it is usual to define the average Lyapunov exponent over all valid initial seeds.
Then it is said that a given discrete dynamical system has chaotic trajectories if the average Lyapunov exponent is positive~\cite{Arnold1986}.


\subsubsection{$H$-ranks}
Begin by establishing a structural matrix whose elements depend only on the sum of their indices.
It is known as a Hankel matrix~\cite{Riedrich1991}.
\begin{mydef}
A {\bfseries Hankel matrix} of depth $k \in \bN$ (also known as a catalecticant, persymmetric matrix~\cite{Muir2003}), is a square matrix
\begin{equation}
M_k =
\begin{pmatrix}
z_0 & z_1 & \dots & z_{k-1} \\
z_1 & z_2 & \dots & z_{k}   \\
\vdots & \vdots & \ddots & \vdots \\
z_{k-1} & z_{k} & \dots & z_{2k-2} \\
\end{pmatrix};
\end{equation}
whose skew-diagonal elements from left to right are constant. $\qed$
\end{mydef}

An orbit $(z_0,\ z_1,\ z_2,\ \ldots)$ of some dynamical system can be used to populate the Hankel matrix.
The so-called Hankel transform of this orbit yields a sequence of determinants $\det ( M_k )$.

\begin{mydef}
The orbit $(z_0,\ z_1,\ z_2,\ \ldots)$ is of $n$-th order if 
\begin{equation}
\det ( M_n ) \neq 0, \qquad \det ( M_{n+k} ) = 0,\ \forall k \in \bN.
\end{equation}
This integer value is known as {\bfseries $H$-rank} and noted $H(z_0,\ z_1,\ z_2,\ \ldots) = n$. $\qed$
\end{mydef}

Hankel rank is an important tool in evaluation of convergence rates, especially in discrete chaotic maps.
The higher the rank, the more complex the calculated trajectory.
The rate of convergence analyzed on a wide scale may provide some insights into the nature of some dynamical attractors~\cite{Landauskas2014}.

\begin{myex}
Let's check the $H$-rank of a simple sequence $(e,\ \pi,\ e, \pi,\ \ldots)$ where odd-positioned elements are equal to $e$ and even-positioned elements are equal to $\pi$.
Proceed by taking some Hankel matrices and their corresponding determinants
\begin{align*}
&	\det(M_1) = \det(e) = e \neq 0; \\
&	\det(M_2) = \det
	\begin{pmatrix}
		e & \pi \\ \pi & e
	\end{pmatrix}
	= e^2 - \pi^2 \neq 0;
\end{align*}
which happen to be non-zero.
The tendency can be noticed
\begin{align*}
& \det(M_3) = \det
\begin{pmatrix}
e & \pi & e \\ \pi & e & \pi \\ e & \pi & e
\end{pmatrix}
=  0; \\
& \det(M_4) = \det
\begin{pmatrix}
	e & \pi & e & \pi \\ \pi & e & \pi & e \\ e & \pi & e & \pi \\ \pi & e & \pi & e
\end{pmatrix}
=  0;
\end{align*}
and similarly $\det ( M_k ) = 0$ for all $k > 2$ because odd-positioned rows of the Hankel matrices are the same as the even-positioned rows.
Then by definition $H(e,\ \pi,\ e, \pi,\ \ldots) = 2$.
It means that the complexity of the sequence in question is of order two.
This fact is understandable because there are precisely two alternating elements in the sequence.
\end{myex}



\subsection[Applications]{Applications of dynamical systems}
There are applications of nonlinear dynamical systems in virtually every field of science.

In the field of medicine and chemistry, applications include genetic control systems~\cite{Li2014}, biological rhythms of various neuronal systems~\cite{Lanfumey2013}, search of cure for cancer or other highly complex diseases~\cite{Dinicola2011}.

In biology, insect outbreaks are modeled and predicted~\cite{Dwyer2004} alongside with management and analysis of animal populations in general~\cite{Williams2002}.

Contemporary engineering includes studies of superconducting circuits and quantum simulations~\cite{Houck2012}, building sophisticated oscillators in meta-materials~\cite{Tassin2012}, measuring mechanical vibrations~\cite{Gatti2014, G.Vinci2013} with extreme precision.

Computer science revolves around improving algorithms of optimization with meta-heuristic approaches to solve structural problems~\cite{Gandomi2011}, multi-label machine learning and classification~\cite{Madjarov2012} and even techniques for using chaos to send encrypted messages~\cite{Yamuna2014}. 

In each and every case, the scientific background varies but is generally closely integrated with the mathematical theory.



\section{Fractals} \label{sec:F}
Many physical systems in nature and many artifacts in human activities are not actually regular geometric shapes.
These shapes do not conform to the standard geometry derived by Euclid. 
Aforementioned shapes belong to some more general geometry which offers more ways of describing, measuring and predicting natural phenomena.
It is known as a fractal geometry~\cite{Bunde1994}.



\subsection[Concept]{The concept of fractality}
In spite of some common sense concepts of fractals, it is not that easy to give a rigorous definition for one. 
According to Mandelbrot~\cite{Mandelbrot1983}, it is ``a rough or fragmented geometric shape that can be split into parts, each of which is (at least approximately) a reduced-size copy of the whole''. 
Sometimes a strict definition for an arbitrary fractal structure is given by the following definition.

\begin{mydef}
	A set or structure $\cF$ whose Hausdorff-Besicovitch dimension (see section \ref{sec:FD}) strictly exceeds its topological dimension is called a {\bfseries fractal}. $\qed$
\end{mydef}

The above definition, no matter how precise, is quite difficult to apply in practice because the Hausdorff-Besicovitch characteristic itself possesses quite a complicated nature.
As a matter of fact, for a lot of widely used fractals it is the case that the exact Hausdorff-Besicovitch dimension is unknown. 
Therefore, some alternative and usually easier to evaluate definitions of fractal structures are used along with some compromises. 

Besides, there exist different types of fractals. 
Two of the most prevalent types are: Iterated function system (IFS) fractals and algebraic fractals.



\subsubsection{Iterated function system fractals}
Iterated function system fractals are obtained by using transformations in a space $(\cH, d)$ of non-empty compact sets $\cH$ 
(possibly subsets of $\bR^2$) and some metric $d \colon \cH^2 \to \bR^+$.
Transformations are conformal and include scaling, translation and rotation~\cite{Martyn2004}. 

The creation of an iterated function system IFS fractal consists of defining a set of multiple conformal transformations 
$\{ \, \omega_k \colon \cH \to \cH \mid k = \ival{1}{n} \, \}$ 
and finding its fixed point $\cF \in \cH$ under the set union operation $W$
\begin{equation} \label{eq:ifs}
\cF = W(\cF) = \bigcup_{k=1}^n w_k(\cF).
\end{equation}
This fixed point $\cF$ is the fractal of interest.
Some of the most famous IFS fractals are the Sierpinski triangle and the Koch snowflake~\cite{Helmberg2007}.



\subsubsection{Algebraic fractals}
Fractals may be created by applying a discrete dynamical system $f \colon \bC \to \bC$ repeatedly. 
Since the iteration $f(z_k) = z_{k+1}$ must be applied indefinitely many times, computational techniques are almost always necessary in order to generate, investigate and see the visual representation of the fractal~\cite{Jampala1992}.
Concepts of Julia set $J$ and Fatou sets $F_k$ are used to describe two complementary sets obtained from an evolutionary function $f$~\cite{Beardon2000}. 

The Fatou set $F_k$, for some $k \in \bN$, of the function $f$ consists of elements with the property that all nearby values behave similarly under the repeated iteration of the function, for example converge to some attractor $\cA_k$.
It is also completely invariant under iterations, meaning
\begin{equation}
f^{-1}(F_k) = f(F_k) = F_k.
\end{equation}

On the contrary, the Julia set $J$ consists of values such that an arbitrarily small perturbation can cause drastic changes in the sequence of iterated function values.
No convergence may be predicted. 
It is the complement of all aforementioned Fatou sets
\begin{equation}
J = \bC \setminus \bigcup_k F_k.
\end{equation}
In this case the Julia set is the fractal entity of interest $J=\cF$.

\begin{myex}
Let's investigate a particular complex-valued function $f(z) = z^2$, $z \in \bC$.
According to the discussed properties of complex numbers (see section \ref{sec:C}), it is understandable that
\begin{equation*}
\forall z \in \bC \quad |z|<1 \Rightarrow 
\lim_{n \to +\infty} f^{\circ n} (z) = \lim_{n \to +\infty} |z|^n e^{i n \Arg(z)} = 0.
\end{equation*}
So the values inside a unit circle converge to a particular attractor 
$\cA_1 = \{ \, 0 \, \}$.
Moreover, they form a Fatou set $F_1 = \{ \, z \in \bC \mid |z| < 1 \, \}$.
Similarly, there is an effect
\begin{equation*}
\forall z \in \bC \quad |z|>1 \Rightarrow 
\lim_{n \to +\infty} f^{\circ n} (z) = \lim_{n \to +\infty} |z|^n e^{i n \Arg(z)} = +\infty;
\end{equation*}
which makes sense only in an extended complex plane.
Simply put, all the values diverge in a similar manner and therefore form a second Fatou set
$F_2 = \{ \, z \in \bC \mid |z| > 1 \, \}$.

The complement $J = \bC \setminus (F_1 \cup F2) = \{ \, z \in \bC \mid |z| = 1 \, \}$ 
is a Julia set.
When the function $f$ acts upon the values of a Julia set, then the angles are doubled 
$\Arg(f(z)) = 2 \Arg(z)$ for $z \in J$.
The operation is also chaotic for the points where complex argument is not a fraction of $2 \pi$.
\end{myex}


\subsection[Properties]{Properties of fractals}
Two of the most apparent properties of fractals are self-similarity and non-integer dimension.
Despite the disagreements on the exact definition, authors usually emphasize at least the basic ideas of self-similarity and an unconventional relationship with the space a fractal is embedded in.

According to widespread research~\cite{Falconer1997} here are some of the characteristics used to describe fractal structures.
In other words, a set $\cF$ that has the property of fractality tends to satisfy the following conditions more often than not:
\begin{enumerate}
	\item $\cF$ has a detailed and fine structure on arbitrary small scales.
	\item $\cF$ is locally and globally irregular in terms of Euclidean geometry.
	\item $\cF$ possesses a property of self-similarity.
	\item $\cF$ has some fractal dimension which is greater than its topological dimension.
	\item $\cF$ can be defined with ease using recursive or iterative methods.
\end{enumerate}



\subsection[Numerical approximations]{Numerical approximations of fractals}
\subsubsection{The deterministic algorithm}
For a set of multiple conformal transformations $\{ \, \omega_k \colon \cH \to \cH \mid k = \ival{1}{n} \, \}$ 
it is possible to approximate the corresponding IFS fractal using \eqref{eq:ifs}
\begin{equation}
\cF_{} = \bigcup_{k=1}^n w_k(\cF).
\end{equation}

Initially an arbitrary compact set $A_0 \in \cH$ is chosen.
Then a sequence of successive elements $A_k = W(A_{k-1})$, $k \in \bN$, is computed, that is $(A_0,\ A_1,\ A_2,\ \ldots, A_n)$.
By theorem of contractive mappings (the proof can be found in~\cite{Barnsley2000}), this sequence converges to the fixed point of the IFS
\begin{equation}
\lim_{n \to +\infty} A_n = \cF.
\end{equation}
If a finite, yet sufficiently large, timespan $n \in \bN$ is chosen, then the resulting set $A_n$ is an approximation of $\cF$.

\begin{figure}[ht]
	\centering
	\includegraphics{../img/mandelbrot}
	\caption[Mandelbrot set]
	{Approximation of the Mandelbrot set. Selected points (color black) do not escape the boundary circle $|z|=2$ after $n=50$ iterations.}
	\label{fig:mandelbrot}
\end{figure}



\subsubsection{The random iteration algorithm}
Once again let's start off with a set of multiple conformal transformations $\{ \, \omega_k \colon \cH \to \cH \mid k = \ival{1}{n} \, \}$.
This time a probability $p_k >0$ is assigned to each transformation $\omega_k$ and additionally $\sum_{k=1}^n p_k = 1$.
The probabilities are typically based on the coefficients of contractivity that certain transformations possess.

Initially an arbitrary point $z_0$ is chosen from any element of $\cH$, so typically $z_0 \in \bC$ or $z_0 \in \bR^2$.
Then during every iteration, a next point $z_t$, $t \in \bN$, is chosen recursively and independently
\begin{equation}
z_t \in \{ \, \omega_1 (z_{t-1}), \omega_2 (z_{t-1}), \ldots , \omega_n (z_{t-1}) \, \};
\end{equation}
with the probability of an event $z_t = \omega_k (z_{t-1})$ being $p_k$.
A resulting collection of such points $\{ \, x_t \mid t \in \bN \, \}$ approximates the fractal $\cF$.
The proof of this fact can be found in~\cite{Barnsley2000}.


\subsubsection{The escape time algorithm} \label{sec:Feta}
This algorithm compares how fast an arbitrary point $z_0 \in \bC$ escapes a given boundary under the action of some dynamical system 
$f \colon \bC \to \bC$.
Intuitively, it is expected that some orbits of the dynamical system settle to some attractor $\cA_k$ slower than others.
For example, initial seeds that are close to the boundary of corresponding Fatou sets $F_k$, for some $k \in \bN$, may demonstrate a complicated behavior~\cite{DarylH.Hepting}.

In fact, it can be the case that some points never converge to any of the attractors.
They are precisely the points of Julia set $J$ and the fractal $\cF$ which is the subject of interest.
During the algorithm, it is investigated whether a finite orbit $(z_0, z_1, \ldots , z_n)$ crosses some boundary around the attracting set $\cA$.
The decision is made based on the following facts:
\begin{enumerate}
	\item If the boundary is crossed at or before the time $n \in \bN$, then $z_0 \not\in \cF$.
	\item If the whole finite orbit $(z_0, z_1, \ldots , z_n)$ stays inside the boundary, then $z_0 \in \cF$.
\end{enumerate}

\begin{myex}
Consider a Mandelbrot set. 
By definition this set consists of complex numbers $c \in \bC$ for which the discrete dynamical system 
$f(z_k)=z_k^2+c$, $k \in \bN$, does not diverge when iterated from a constant initial seed $z_0 = 0$.

Choose a complex boundary circle $|z|=2$ and state that the system diverges if this boundary is crossed (this fact is proved in~\cite{Mandelbrot1983}).
Then investigate different values $c \in \bC$ for the orbits of length $n=50$.
The visual result is presented in figure~\ref{fig:mandelbrot}. 
The parameters $c \in \bC$ that do not cause the suspected divergence are painted black.
This is an approximation of the Mandelbrot set.
\end{myex}



\subsection[Applications]{Applications of fractals} \label{sec:Fapps}
Fractal geometry has influenced many areas of contemporary science such as biology, astrophysics, engineering and especially techniques in computer graphics.

Fractals appear in biological sciences as representations of animal movement~\cite{Cole1995} and the structure of their habitat~\cite{Nams2004}.
Additionally, the basic architecture of a chromosome is believed to be fractal-like~\cite{Mirny2011} similarly to the DNA sequences~\cite{Voss1992}.

In astrophysics, the formation process of interstellar clouds is believed to resemble a formation process of fractals~\cite{Elmegreen2002}.
The universe appears as if it is a classically self-similar random process at all astrophysical scales~\cite{Iovane2004}.
Galaxy samples are considered to possess fractal properties~\cite{Labini1996}. 

There are applications of fractals in the field of engineering.
In antennas, the fractal shapes are translated into the electromagnetic behavior~\cite{Puente-Baliarda1998}.
Self-similar processes and multifractal processes are needed in order to model a network traffic~\cite{Taqqu1997}.
The growth or urban ares can be described using fractal techniques~\cite{Batty1987}.

Finally, fractals are used in computer science. 
Image compression schemes use fractal algorithms to compress graphic files~\cite{FISHER1994}.
Artists use self-similar forms to create, model and present textured landscapes, mountain ranges and coastlines~\cite{Lantuit2009}.
Fractal art and architecture is believed to possess an aesthetic quality based on their visual complexity~\cite{Taylor2006}.

Overall, fractal concepts can be used to model natural objects.
They allow to mathematically define the used environment with high accuracy and precision.



\section[Motivation]{Motivation of thesis}
In order to accomplish the aims of this thesis, it is adequate to choose a family of complex discrete dynamical systems.
The set of complex numbers $\bC$ is a substantial generalization over a set of real numbers $\bR$.
Consequently, more advanced tools can be adopted and more profound insights can be made if a complex-valued function $f$ is chosen.

The aforementioned systems are to be thoroughly evaluated in terms of complexity and transient dynamics.
Discrete dynamical systems $z_{n+1} = f(z_n)$, where $n \in \bN$, may exhibit highly non-trivial behavior that is not initially apparent.
Advanced techniques are required to describe and measure the chaotic tendencies.

Attracting basins and their boundaries are to be evaluated in terms of fractal geometry.
Fractals $\cF$ are the results of chaos.
These sets may form structures that are fully connected yet discontinuous everywhere, and have non-integer dimensions.

Finally, numerical results are to be compared and conclusions are to be made.