function w = Wada(  A, varargin  )
% Wada coefficient Wada(A,[h])
% for the basins of attraction A
% and counting depth h

optargs = {100};
optargs(1:nargin-1) = varargin;
[h] = optargs{:};

% Different attractors
a = unique(A);
a = a(a ~= 0);

for k = h:-1:1;
    [x(k), y(k)] = DCount(k+1);
end;

w = y ./ x;
w = mean(w);


function [x, y] = DCount(k)
% Wada count
% of k-by-k boxes
% for basins A

M = im2col(A, [k k], 'distinct');
fM = zeros([1 size(M,2)]);

for n = 1:numel(a);
    fM = fM + (sum(M == a(n), 1) > 0);
end;

x = sum(fM > 1, 2);
y = sum(fM > 2, 2);
    
end

end