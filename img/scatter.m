clear;
load('../calc/NewtonWD.mat');

for k = 1;
switch k;
    case 1;
        str = 'DW';
        plot(D(:), W(:), '.k');
        xlabel('$D(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
        ylabel('$W(\mathcal{F})$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
end;

daspect('auto');
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.08 0.14 0.9 0.82],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') str '.png'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 15 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 8 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-dpng', fname);

end;