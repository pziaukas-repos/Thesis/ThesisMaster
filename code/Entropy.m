function s = Entropy(  A, varargin  )
% Entropy coefficient Entropy(A,[h])
% for the basins of attraction A
% and counting depth h

optargs = {100};
optargs(1:nargin-1) = varargin;
[h] = optargs{:};

% Different attractors
a = unique(A);
a = a(a ~= 0);

for k = h:-1:1;
    s(k) = ECount(k+1);
end;

s = mean(s);


function s = ECount(k)
% Entropy count
% of k-by-k boxes
% for basins A

M = im2col(A, [k k], 'distinct');
boxNumber = size(M,2);
zeroM = sum(M == 0, 1);
fM = zeros([1 boxNumber]);

for n = 1:numel(a);
    thisM = sum(M == a(n), 1);
    totalM = k^2 - zeroM;
    thisM(thisM == 0) = totalM(thisM == 0);
    fM = fM - (thisM ./ totalM) .* log(thisM ./ totalM);
end;

s = sum(fM) / boxNumber;
    
end

end