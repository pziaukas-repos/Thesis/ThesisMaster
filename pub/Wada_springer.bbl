\begin{thebibliography}{10}
\providecommand{\url}[1]{{#1}}
\providecommand{\urlprefix}{URL }
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{DOI~\discretionary{}{}{}#1}\else
  \providecommand{\doi}{DOI~\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi

\bibitem{Aguirre2002}
Aguirre, J., Sanju{\'{a}}n, M.A.: {Unpredictable behavior in the Duffing
  oscillator: Wada basins}.
\newblock Physica D: Nonlinear Phenomena \textbf{171}(1-2), 41--51 (2002).
\newblock \doi{10.1016/S0167-2789(02)00565-1}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0167278902005651}

\bibitem{Amrein2014}
Amrein, M., Wihler, T.P.: {An adaptive Newton-method based on a dynamical
  systems approach}.
\newblock Communications in Nonlinear Science and Numerical Simulation
  \textbf{19}(9), 2958--2973 (2014).
\newblock \doi{10.1016/j.cnsns.2014.02.010}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S100757041400063X}

\bibitem{Atkinson1978}
Atkinson, K.E.: {An Introduction to Numerical Analysis}.
\newblock Wiley (1978).
\newblock
  \urlprefix\url{https://books.google.lt/books/about/An{\_}introduction{\_}to{\_}numerical{\_}analysis.html?id=ByPpQ1nt3esC{\&}pgis=1}

\bibitem{Barnsley2000}
Barnsley, M.F., Rising, H.: {Fractals Everywhere}.
\newblock Morgan Kaufmann (2000).
\newblock
  \urlprefix\url{https://books.google.lt/books/about/Fractals{\_}Everywhere.html?id=oh7NoePgmOIC{\&}pgis=1}

\bibitem{Breban2005}
Breban, R., Nusse, H.E.: {On the creation of Wada basins in interval maps
  through fixed point tangent bifurcation}.
\newblock Physica D: Nonlinear Phenomena \textbf{207}(1-2), 52--63 (2005).
\newblock \doi{10.1016/j.physd.2005.05.012}.
\newblock \urlprefix\url{http://escholarship.org/uc/item/3j38277x{\#}page-6}

\bibitem{Cartwright1999}
Cartwright, J.H.: {Newton maps: fractals from Newton's method for the circle
  map}.
\newblock Computers {\&} Graphics \textbf{23}(4), 607--612 (1999).
\newblock \doi{10.1016/S0097-8493(99)00078-3}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0097849399000783}

\bibitem{Cayley1879}
Cayley, A.: {Application of the Newton-Fourier method to an imaginary root of
  an equation}.
\newblock Quart. J. Pure Appl. Math  (1879).
\newblock
  \urlprefix\url{https://scholar.google.lt/scholar?q=A.Cayley{\%}2C+Application+of+the+Newton-Fourier+Mathod+to+an+imaginary+root+of+equation{\&}btnG={\&}hl=en{\&}as{\_}sdt=0{\%}2C5{\#}0}

\bibitem{ChandraSekhar2010}
{Chandra Sekhar}, D., Ganguli, R.: {Fractal boundaries of basin of attraction
  of Newton–Raphson method in helicopter trim}.
\newblock Computers {\&} Mathematics with Applications \textbf{60}(10),
  2834--2858 (2010).
\newblock \doi{10.1016/j.camwa.2010.09.040}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0898122110007364}

\bibitem{Daza2015a}
Daza, A., Wagemakers, A., Sanju{\'{a}}n, M.A.F., Yorke, J.A.: {Testing for
  Basins of Wada.}
\newblock Scientific reports \textbf{5}, 16,579 (2015).
\newblock \doi{10.1038/srep16579}.
\newblock
  \urlprefix\url{http://www.nature.com/srep/2015/151110/srep16579/full/srep16579.html}

\bibitem{Drexler1995}
Drexler, M., Sobey, I., Bracher, C.: {On the fractal characteristics of a
  stabilised Newton method}.
\newblock Oxford University Computing Laboratory (1995).
\newblock \urlprefix\url{http://eprints.maths.ox.ac.uk/1332/1/NA-95-26.pdf}

\bibitem{Drexler1996a}
Drexler, M., Sobey, I., Bracher, C.: {Fractal Characteristics of Newton's
  Method on Polynomials}.
\newblock Oxford University Computer Laboratory (1996).
\newblock \urlprefix\url{http://eprints.maths.ox.ac.uk/1323/}

\bibitem{Epureanu1998}
Epureanu, B.I., Greenside, H.S.: {Fractal Basins of Attraction Associated with
  a Damped Newton's Method}.
\newblock SIAM Review \textbf{40}(1), 102--109 (1998).
\newblock \doi{10.1137/S0036144596310033}.
\newblock
  \urlprefix\url{http://epubs.siam.org/doi/abs/10.1137/S0036144596310033}

\bibitem{Fatou1919}
Fatou, P.: {Sur les {\'{e}}quations fonctionnelles}.
\newblock Bulletin de la Soci{\'{e}}t{\'{e}} math{\'{e}}matique de France
  (1919).
\newblock \urlprefix\url{https://eudml.org/doc/86391}

\bibitem{Frame2007}
Frame, M., Neger, N.: {Newton's Method and the Wada Property: A Graphical
  Approach}.
\newblock The College Mathematics Journal \textbf{38}(3), 192--204 (2007).
\newblock
  \urlprefix\url{https://www.researchgate.net/publication/233615488{\_}Newton's{\_}Method{\_}and{\_}the{\_}Wada{\_}Property{\_}A{\_}Graphical{\_}Approach}

\bibitem{GILBERT2001}
Gilbert, W.J.: {Generalizations of Newton'S Method}.
\newblock Fractals \textbf{09}(03), 251--262 (2001).
\newblock \doi{10.1142/S0218348X01000737}.
\newblock
  \urlprefix\url{http://www.worldscientific.com/doi/abs/10.1142/S0218348X01000737}

\bibitem{Holt1984}
Holt, R., Schwartz, I.: {Newton's method as a dynamical system: Global
  convergence and predictability}.
\newblock Physics Letters A \textbf{105}(7), 327--333 (1984).
\newblock \doi{10.1016/0375-9601(84)90273-1}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/0375960184902731}

\bibitem{Hubbard2001}
Hubbard, J., Schleicher, D., Sutherland, S.: {How to find all roots of complex
  polynomials by Newton's method}.
\newblock Inventiones Mathematicae \textbf{146}(1), 1--33 (2001).
\newblock \doi{10.1007/s002220100149}.
\newblock \urlprefix\url{http://link.springer.com/10.1007/s002220100149}

\bibitem{Jr1991}
Jr, P.S.: {Newton's method and fractal patterns}.
\newblock Applications of Calculus  (1991).
\newblock \urlprefix\url{http://www.zebragraph.com/PolyFinal{\_}files/99716
  copy.pdf}

\bibitem{Julia}
Julia, G.: {M{\'{e}}moire sur l'iteration des fonctions rationnelles}.
\newblock Journal de Math{\'{e}}matiques Pures et Appliqu{\'{e}}es \textbf{8},
  47--245 (1918).
\newblock \urlprefix\url{https://eudml.org/doc/234994}

\bibitem{Kennedy1991}
Kennedy, J., Yorke, J.A.: {Basins of Wada}.
\newblock Physica D: Nonlinear Phenomena \textbf{51}(1-3), 213--225 (1991).
\newblock \doi{10.1016/0167-2789(91)90234-Z}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/016727899190234Z}

\bibitem{Krause1975}
Krause, E.F.: {Taxicab Geometry: An Adventure in Non-Euclidean Geometry}.
\newblock Courier Corporation (1975).
\newblock
  \urlprefix\url{https://books.google.com/books?id=IW7ICV0QXWwC{\&}pgis=1}

\bibitem{Nusse1996}
Nusse, H.E., Yorke, J.A.: {Wada basin boundaries and basin cells}.
\newblock Physica D: Nonlinear Phenomena \textbf{90}(3), 242--261 (1996).
\newblock \doi{10.1016/0167-2789(95)00249-9}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/0167278995002499}

\bibitem{Ott2002}
Ott, E.: {Chaos in Dynamical Systems}.
\newblock Cambridge University Press (2002).
\newblock
  \urlprefix\url{https://books.google.com/books?id=nOLx--zzHSgC{\&}pgis=1}

\bibitem{POON1996}
Poon, L., Campos, J., Edward, O., Grebogi, C.: {Wada Basin Boundaries in
  Chaotic Scattering}.
\newblock International Journal of Bifurcation and Chaos \textbf{06}(02),
  251--265 (1996).
\newblock \doi{10.1142/S0218127496000035}.
\newblock
  \urlprefix\url{http://www.worldscientific.com/doi/abs/10.1142/S0218127496000035}

\bibitem{PORTELA2007}
Portela, S., Iber, E.: {Fractal and Wada Exit Basin Boundaries in Tokamaks}.
\newblock International Journal of Bifurcation and Chaos \textbf{17}(11),
  4067--4079 (2007).
\newblock \doi{10.1142/S021812740701986X}.
\newblock
  \urlprefix\url{http://www.worldscientific.com/doi/abs/10.1142/S021812740701986X?journalCode=ijbc}

\bibitem{Schrder1870}
Schr{\"{o}}der, E.: {Ueber unendlich viele Algorithmen zur Aufl{\"{o}}sung der
  Gleichungen}.
\newblock Mathematische Annalen \textbf{2}(2), 317--365 (1870).
\newblock \doi{10.1007/BF01444024}.
\newblock \urlprefix\url{http://link.springer.com/10.1007/BF01444024}

\bibitem{Drexler1996}
Sobey, I.J.: {Characteristics of Newton's Method on Polynomials}.
\newblock Oxford Computer Lab (1996).
\newblock \urlprefix\url{http://eprints.maths.ox.ac.uk/1323/1/NA-96-14.pdf}

\bibitem{Susanto2009}
Susanto, H., Karjanto, N.: {Newton's method's basins of attraction revisited}.
\newblock Applied Mathematics and Computation \textbf{215}(3), 1084--1090
  (2009).
\newblock \doi{10.1016/j.amc.2009.06.041}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0096300309006134}

\bibitem{Sweet1999}
Sweet, D., Ott, E., Yorke, J.A.: {Topology in chaotic scattering}.
\newblock Nature \textbf{399}(6734), 315--316 (1999).
\newblock \doi{10.1038/20573}.
\newblock \urlprefix\url{http://dx.doi.org/10.1038/20573}

\bibitem{Vandermeer2004}
Vandermeer, J.: {Wada basins and qualitative unpredictability in ecological
  models: a graphical interpretation}.
\newblock Ecological Modelling \textbf{176}(1-2), 65--74 (2004).
\newblock \doi{10.1016/j.ecolmodel.2003.10.028}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0304380003005568}

\bibitem{Vandermeer2001}
Vandermeer, J., Stone, L., Blasius, B.: {Categories of chaos and fractal basin
  boundaries in forced predator–prey models}.
\newblock Chaos, Solitons {\&} Fractals \textbf{12}(2), 265--276 (2001).
\newblock \doi{10.1016/S0960-0779(00)00111-9}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0960077900001119}

\bibitem{Walsh1995}
Walsh, J.: {The dynamics of Newton's method for cubic polynomials}.
\newblock The College Mathematics Journal  (1995).
\newblock
  \urlprefix\url{http://www.geofhagopian.net/M1A/M1A-F10/NewtonsMeth{\_}Cubics{\_}2687287.pdf}

\bibitem{Wang2007}
Wang, X., Yu, X.: {Julia sets for the standard Newton's method, Halley's
  method, and Schr{\"{o}}der's method}.
\newblock Applied Mathematics and Computation \textbf{189}(2), 1186--1195
  (2007).
\newblock \doi{10.1016/j.amc.2006.12.002}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0096300306017024}

\bibitem{Zhang2012}
Zhang, Y., Luo, G.: {Unpredictability of the Wada property in the parameter
  plane}.
\newblock Physics Letters A \textbf{376}(45), 3060--3066 (2012).
\newblock \doi{10.1016/j.physleta.2012.08.015}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0375960112009139}

\bibitem{Zhang2013}
Zhang, Y., Luo, G.: {Wada bifurcations and partially Wada basin boundaries in a
  two-dimensional cubic map}.
\newblock Physics Letters A \textbf{377}(18), 1274--1281 (2013).
\newblock \doi{10.1016/j.physleta.2013.03.027}.
\newblock
  \urlprefix\url{http://www.sciencedirect.com/science/article/pii/S0375960113002983}

\end{thebibliography}
