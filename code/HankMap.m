function A = HankMap( MM, varargin )
% Map of Hankel's ranks

optargs = {.001};
optargs(1:nargin-1) = varargin;
[eps] = optargs{:};

S = size(MM);
maxRank = floor((S(3)+1)/2);
A = zeros(S(1:2));

for n1 = 1:S(1);
    for n2 = 1:S(2);
        A(n1,n2) = HankRank(MM(n1,n2,:));
    end;
end;


function r = HankRank( V )
% Rank of the Hankel's matrix
% V - vector
% eps - sensitivity

if (isfinite(V(:)));
    M = hankel(V(1:maxRank), V(maxRank:2*maxRank-1));
    r = sum(abs(svd(M)) > eps);
else
    r = maxRank;
end;

end

end

