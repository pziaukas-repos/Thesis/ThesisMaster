clear;
load('../calc/NewtonWD.mat');

% maximum difference betwwen normalized D and W
outcome = mat2gray(D)-mat2gray(W);
outcomeRez = 0;
for i = 1:size(outcome,1);
    for j = 1:size(outcome,2);
        if (outcome(i,j) > outcomeRez);
            outcomeRez = outcome(i,j);
            iRez = i;
            jRez = j;
        end;
    end; 
end;

rRez = R(iRez, jRez);
fZ = Newton(Z, n, rRez);
A = NewtonAttractors(fZ);
Paint(A);
fprintf('d = %.2f;   w = %.2f   @   p = %.2f\n', Dimension(A), Wada(A), rRez);