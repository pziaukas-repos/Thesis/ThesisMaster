clear;
load('../calc/NewtonWD.mat');
addpath('../code');



fZ = Newton(Z, n);
A = NewtonAttractors(fZ);
F = edge(A);
for b = [20 40];
bFill = @(M) kron(sum(M,1)==0, ones(size(M,1),1));
fF = colfilt(F, [b b], 'distinct', bFill);
sum(sum(im2col(F, [b b], 'distinct'),1)>0) % count N(eps)

cmap = [.9 .1 .1; .9 .9 .1; .1 .1 .9; 1 1 1];
imshow(label2rgb(A .* fF + 4*F, cmap, 'k'), 'XData', xMin:d:xMax, 'YData', yMin:d:yMax);
daspect('auto');
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.14 0.1 0.85 0.85],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') sprintf('%02d', b) '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 7 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 7 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;