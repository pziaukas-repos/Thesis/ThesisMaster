clear;
addpath('../code');

n = 35;
h = .1;
range = 0:n-1;
rangeInterpolated = 0:h:n-1;

for r = [1 0.99];

fZ = zeros(n,3);
Z = -0.793 + .001*(-1:1)*1i;
for k = 1:n;
    fZ(k,:) = Z;
    Z = Newton(Z, r);
end;
fZ = wrapTo2Pi(angle(fZ));

plot(rangeInterpolated, spline(range, fZ', rangeInterpolated), 'LineWidth', 2);
daspect('auto');
xlabel('$n$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
ylabel('Arg$(z_n)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
switch r;
    case 1;
        axis([0 n -.5 6]);
        set(gca,...
            'YTick', [0 pi/2 pi 3*pi/2 2*pi],...
            'YTickLabel', {'0', '0.50\pi', '\pi', '1.50\pi', '2\pi'});
    otherwise;
        axis([0 n 3.10 3.18]);
        set(gca,...
            'YTick', [0.99*pi pi 1.01*pi],...
            'YTickLabel', {'0.99\pi', '\pi', '1.01\pi'}); 
end;
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.11 0.22 0.87 0.75],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') sprintf('%03d_%03di', round(100*real(r)), round(100*imag(r))) '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 15 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 5 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;