clear;
load('../calc/NewtonWD.mat');
zoom = 3;

for k = 1:2;
switch k;
    case 1;
        str = 'W';
        M = interp2(W,zoom);
        mMin = 0;
        mMax = 1;
    case 2;
        str = 'D';
        M = interp2(D,zoom);
        mMin = 1;
        mMax = max(M(:));
end;

imshow(uint8(255*mat2gray(M)), 'XData', pMin:h:pMax, 'YData', qMin:h:qMax);
colormap jet;
daspect('auto');
cbar = colorbar('YTickLabel', {round(100 * (mMin:.2*(mMax-mMin):mMax))/100});
xlabel('$\Re(\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
ylabel('$\Im(\alpha)$', 'Interpreter', 'LaTeX', 'FontName', 'Times New Roman', 'FontSize', 10);
set(gca,...
	 'Visible', 'on',...
	 'Box', 'off',...
	 'YDir', 'normal',...
	 'Position', [0.11 0.1 0.75 0.87],...
	 'FontName', 'Times New Roman', 'FontSize', 11);
fname = [mfilename('fullpath') str '.eps'];
set(gcf, 'Color', [1 1 1]);
figureW = floor(96 * 15 / 2.54); % screenDPI * widthCM / inchCM
figureH = floor(96 * 13 / 2.54);
set(gcf, 'Position', [50 50 figureW figureH]);
set(gcf, 'PaperPositionMode', 'auto');
print(gcf, '-r300', '-depsc', fname);

end;