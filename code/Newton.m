function Z = Newton( Z, varargin )
% Newton's method 
% Newton(Z, [n, a, P])
% for polynomial P 
% n iterations
% Z = Z - a*P(Z)/P'(Z) 

optargs = {1, 1, [1 0 0 -1]};
optargs(1:nargin-1) = varargin;
[n ,a, P] = optargs{:};

D = polyder(P);

for k = 1:n;
    Z = Z - a * polyval(P, Z) ./ polyval(D, Z);
%     Z = Z + normrnd(0,.1,S) .* exp(1i*2*pi*rand(S));
end;

end

